process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true
window.WINDOW_ID = 'start'
require('./lib/console-extend')
const { initApp, initComponents, startApp, reloadApp } = require('./lib/cp-manager')

const excludePackages = ['builder', 'pdf', 'test']

document.addEventListener('DOMContentLoaded', function () {
  initApp(excludePackages)
  initComponents('layout')
  initComponents('reflow')
  startApp()
  // reload Page ctrl + r
  document.addEventListener('reloadApp', function () {
    reloadApp()
    document.body.dispatch$('reloadApp')
    console.clear()
    initApp(excludePackages)
    initComponents('layout')
    initComponents('reflow')
    startApp()
  })
  document.addEventListener('keydown', function (event) {
    if (event.ctrlKey && event.keyCode === 82) {
      document.body.trigger('reloadApp')
    }
  })
  // devTools
  document.addEventListener('keydown', function (event) {
    if (event.ctrlKey && event.shiftKey && event.keyCode === 73) {
      console.devTools.toogle()
    }
  })
})
