# proto

  - références croisée + toc
  - erreur et numéro de ligne (ajouter track)
  - structure d'un projet
  - dialogue d'ouverture d'un projet
  - cohérence d'un thème (font, couleur, taille...)

  - gestionnaire des préférences utilisateurs
  - locales app et locale document (reflox, layout)
  - plugins + (add command, keymaps)
    - appli
      - themes
    - reflow
      - themes
    - layout
      - themes
  - export web et pwa
  - builder de composants
  - changer d’algorithme pour la création des pdf => une seule police


## bascule entre fichiers
https://github.com/Microsoft/monaco-editor/blob/bad3c34056624dca34ac8be5028ae3454172125c/website/playground/playground.js#L108

```javascript
function changeTab(selectedTabNode, desiredModelId) {
		for (var i = 0; i < tabArea.childNodes.length; i++) {
			var child = tabArea.childNodes[i];
			if (/tab/.test(child.className)) {
				child.className = 'tab';
			}
		}
		selectedTabNode.className = 'tab active';

		var currentState = editor.saveViewState();

		var currentModel = editor.getModel();
		if (currentModel === data.js.model) {
			data.js.state = currentState;
		} else if (currentModel === data.css.model) {
			data.css.state = currentState;
		} else if (currentModel === data.html.model) {
			data.html.state = currentState;
		}

		editor.setModel(data[desiredModelId].model);
		editor.restoreViewState(data[desiredModelId].state);
		editor.focus();
	}
```
## serveur de registre npm
https://verdaccio.org/
