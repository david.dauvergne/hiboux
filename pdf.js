process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true
const electron = require('electron')
const remote = electron.remote
require('./lib/console-extend')
const { initApp, initComponents, startApp } = require('./lib/cp-manager')

const excludePackages = ['app', 'builder', 'test']

document.addEventListener('DOMContentLoaded', function () {
  const pdfWindow = remote.getCurrentWindow()
  initApp(excludePackages)
  initComponents('layout')
  startApp()
  document.body.render(pdfWindow)
})
