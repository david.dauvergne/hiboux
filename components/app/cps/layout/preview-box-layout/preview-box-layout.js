const layoutElements = {}

const _model = {
  title: 'title book',
  unit: 'mm',
  page: {
    width: 120,
    height: 180,
    column: 140, // column => body.width + margins.gap
    backgroundColor: 'white'
  },
  body: { width: 80, height: 120 },
  margins: {
    top: 20, // Blanc de tête
    bottom: 40, // Blanc de pied
    inner: 10, // Petit fond
    outer: 30, // Grand fond,
    gap: 60, // Math.max(outer, inner) * 2 => Gouttière entre colonnes
    gapinner: 20, // Math.abs(outer - inner)
    gap2inner: 40 // gapinner * 2 => Gouttière entre 2 petits fonds
  },
  // counter type
  // arabic: arabic numerals
  // roman: lowercase roman numerals
  // Roman: uppercase roman numerals
  // alpha: lowercase letters
  // Alpha: uppercase letters
  counter: { start: 0, reset: false, type: 'arabic' },
  templates: {
    right: {
      top: 'default-top',
      bottom: 'default-bottom',
      inner: null,
      outer: null
    },
    left: {
      top: 'default-top',
      bottom: 'default-bottom',
      inner: null,
      outer: null
    },
    pages: {
      right: {
        start: true,
        end: false,
        first: { top: false, bottom: false },
        last: { top: true, bottom: true }
      },
      left: {
        start: false,
        end: true,
        first: { top: false, bottom: false },
        last: { top: false, bottom: false }
      }
    }
  }
}

module.exports = {
  events: {
    zoomminus: {
      click: function (e) {
        if (this.zoomLayout > 0) {
          this.zoomLayout = this.zoomLayout - 0.1
          this.els.pager.style.zoom = this.zoomLayout
        }
      }
    },
    zommplus: {
      click: function (e) {
        this.zoomLayout = this.zoomLayout + 0.1
        this.els.pager.style.zoom = this.zoomLayout
      }
    },
    toleft: {
      click: function (e) {
        this.dispatch$('scrollToPage', 'left')
      }
    },
    toright: {
      click: function (e) {
        this.dispatch$('scrollToPage', 'right')
      }
    }
  },
  properties: {
    zoomLayout: 1,
    models: {}
  },
  methods: {
    $p_scollToLeft: function (data) {
      if (data[0] === 'disabled') {
        this.els.toleft.setAttribute('disabled', 'disabled')
      } else {
        this.els.toleft.removeAttribute('disabled')
      }
    },
    $p_scollToRight: function (data) {
      if (data[0] === 'disabled') {
        this.els.toright.setAttribute('disabled', 'disabled')
      } else {
        this.els.toright.removeAttribute('disabled')
      }
    },
    $p_editorContent: function (data) {
      if (this.style.display === 'block' && data[0]) {
        const d = data[0]
        let layouPagestEl
        if (layoutElements[d.uri]) {
          layouPagestEl = layoutElements[d.uri]
        } else {
          const model = JSON.parse(JSON.stringify(_model))
          model.uri = d.uri
          this.models[d.uri] = model
          const { element } = this.renderTpl('layout', this.models[d.uri])
          layouPagestEl = element
          layoutElements[d.uri] = element
        }
        this.els.pager.style.width = `${this.models[d.uri].page.width}${this.models[d.uri].unit}`
        this.els.pager.style.height = `${this.models[d.uri].page.height}${this.models[d.uri].unit}`
        layouPagestEl.render(d)
      }
    },
    $p_displayMode: function (data) {
      if (data[0]) {
        this.style.display = data[0] === 'layout' ? 'block' : 'none'
      }
    }
  }
}
