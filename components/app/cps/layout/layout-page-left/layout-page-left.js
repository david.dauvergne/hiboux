module.exports = {
  properties: {
    pagination: 'left'
  },
  methods: {
    assignTPL: function (model, theme, pageNumber, index, pages) {
      model.page.number = pageNumber
      model.page.pagination = this.pagination
      const t = model.templates[this.pagination]
      const l = model.templates.pages[this.pagination]
      let top = true
      let bottom = true
      if (index === 0) {
        if (!l.first.top) {
          top = false
        }
        if (!l.first.bottom) {
          bottom = false
        }
      }
      if (index === pages) {
        if (!l.last.top) {
          top = false
        }
        if (!l.last.bottom) {
          bottom = false
        }
      }

      if (t.top && top) {
        this.els.top.renderTpl(theme.templates[t.top][this.pagination], model, 'layout')
      }
      if (t.bottom && bottom) {
        this.els.bottom.renderTpl(theme.templates[t.bottom][this.pagination], model, 'layout')
      }
      if (t.inner) {
        this.els.inner.renderTpl(theme.templates[t.inner][this.pagination], model, 'layout')
      }
      if (t.outer) {
        this.els.outer.renderTpl(theme.templates[t.outer][this.pagination], model, 'layout')
      }
    }
  }
}
