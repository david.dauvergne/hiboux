
const romanKey = ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM', '', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC', '', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX']

function romanize (num, lower) {
  const digits = String(+num).split('')
  let roman = ''
  let val = null
  let i = 3
  while (i--) {
    roman = (romanKey[+digits.pop() + (i * 10)] || '') + roman
  }

  val = Array(+digits.join('') + 1).join('M') + roman

  if (lower) {
    return val.toLowerCase()
  }
  return val
}

function aphanize (num, lower) {
  let val = ''
  let t

  while (num > 0) {
    t = (num - 1) % 26
    val = String.fromCharCode(65 + t) + val
    num = (num - t) / 26 | 0
  }

  if (lower) {
    return val.toLowerCase()
  }
  return val
}

module.exports = {
  dom: {
    insert: function () {
      this.pagecolumn = this.els.pagecolumn.getBoundingClientRect().width
      this.start = this.data.counter.start
      this.pages = 0
      if (this.data.counter.reset) {
        this.start = 0
      }
      this.startPagePosition()
      this.body = {
        type: 'tag',
        name: 'layout-body',
        coulumnStart: [],
        attributes: {
          el: 'body',
          style: `columns:${this.data.body.width}${this.data.unit};top:${this.data.margins.top}${this.data.unit};height:${this.data.body.height}${this.data.unit};column-gap:${this.data.margins.gap}${this.data.unit};margin-left:${this.bodyMarginLeft};`
        },
        children: []
      }
      this.vDom = JSON.parse(JSON.stringify(this.body))
      this.els.body.__$ns = 'layout'
      this.els.body.data = null
    }
  },
  events: {
    body: {
      mousedown: function (e) {
        if (this.removeHighlights) {
          this.removeHighlights()
          this.removeHighlights = null
        }
      },
      mouseup: function (e) {
        const sel = window.getSelection()
        if (sel.isCollapsed) {
          const range = document.caretRangeFromPoint(e.clientX, e.clientY)
          const textNode = range.startContainer
          const offset = range.startOffset - 1
          if (textNode && textNode.nodeType === 3) {
            const parentNode = this._getParent(textNode)
            const column = this._getColumn(textNode, parentNode, offset)
            this.dispatch$('setPosition', {
              lineNumber: (parentNode.getAttribute('__line') * 1) + 1,
              column: column + 1
            })
          }
        } else {
          if (sel.anchorNode && sel.anchorNode.nodeType === 3 && sel.focusNode && sel.focusNode.nodeType === 3) {
            const anchorParentNode = this._getParent(sel.anchorNode)
            const anchorColumn = this._getColumn(sel.anchorNode, anchorParentNode, sel.anchorOffset)
            const focusParentNode = this._getParent(sel.focusNode)
            const focusColumn = this._getColumn(sel.focusNode, focusParentNode, sel.focusOffset)
            const ancharLine = (anchorParentNode.getAttribute('__line') * 1) + 1
            const focusLine = (focusParentNode.getAttribute('__line') * 1) + 1
            this.dispatch$('setRange', {
              anchorLineNumber: ancharLine,
              anchorColumn: anchorColumn,
              focusLineNumber: focusLine,
              focusColumn: focusColumn
            })
          }
        }
      }
    }
  },
  properties: {
    pageView: 0,
    $p_scollToLeft: '',
    $p_scollToRight: ''
  },
  methods: {
    _getParent: function (node) {
      if (node.parentNode.getAttribute('el') === 'content') {
        return node.parentNode.__$refparent
      }
      return node.parentNode
    },
    _getColumn: function (textNode, parentNode, offset) {
      const indexNode = Array.prototype.indexOf.call(textNode.parentNode.childNodes, textNode)
      return parentNode.coulumnStart[indexNode] + offset
    },
    $scrollToPage: function (direction) {
      if (direction === 'left') {
        this._scrollToPage(this.pageView - 1)
      } else if (direction === 'right') {
        this._scrollToPage(this.pageView + 1)
      }
    },
    startPagePosition: function () {
      const p = this.data.templates.pages
      this.startPos = 'right'
      this.pageOrder = ['right', 'left']
      if ((!p.right.start && p.right.start) || // start left
        (!p.right.start && !p.right.start && this.pageStart % 2 !== 0)) { // relative page number
        this.startPos = 'left'
        this.pageOrder = ['left', 'right']
      }
      // start page left => outer, right => inner + gap2inner
      if (this.startPos === 'right') {
        this.bodyMarginLeft = `${this.data.margins.inner + this.data.margins.gap2inner}${this.data.unit}`
      } else {
        this.bodyMarginLeft = `${this.data.margins.outer}${this.data.unit}`
      }
    },
    getPageNumber: function (el) {
      return Math.floor(((el.offsetLeft - this.startElLeft) / this.pagecolumn) + 1.01)
    },
    getPageNumberCounter: function (page) {
      const p = this.start + page
      // counter type
      // arabic: arabic numerals
      // roman: lowercase roman numerals
      // Roman: uppercase roman numerals
      // alpha: lowercase letters
      // Alpha: uppercase letters
      switch (this.data.counter.type) {
        case 'roman':
          return romanize(p, true)
        case 'Roman':
          return romanize(p)
        case 'alpha':
          return aphanize(p, true)
        case 'Alpha':
          return aphanize(p)
        default:
          return p
      }
    },
    _scrollToPage: function (page) {
      if (this.pageView !== page) {
        const order = page % 2 === 0 ? this.pageOrder[1] : this.pageOrder[0]
        let width = (page * this.data.page.column) - this.data.page.column
        if (order === 'right') {
          width = width + this.data.margins.gapinner
        }
        if (this.startPos === 'right') {
          width = width + this.data.margins.gapinner
        }
        this.parentNode.style.transform = `translateX(-${width}${this.data.unit})`
        this.pageView = page
        let scollToLeft = ''
        let scollToRight = ''

        if (page === 1) {
          scollToLeft = 'disabled'
        }
        if (page === this.pages) {
          scollToRight = 'disabled'
        }
        this.p_scollToLeft = scollToLeft
        this.p_scollToRight = scollToRight
      }
    },
    render: function (data, noMoveToCaret) {
      if (this.removeHighlights) {
        this.removeHighlights()
      }
      const body = JSON.parse(JSON.stringify(this.body))
      body.children = data.content.children

      const patch = window.M.diff(this.vDom, body)
      patch(this.els.body)
      this.vDom = body

      // create layout pages
      this.startElLeft = this.els.body.firstChild.offsetLeft

      let pages = this.getPageNumber(this.els.body.querySelector('__last__'))
      const lastPagePagination = pages % 2 === 0 ? this.pageOrder[1] : this.pageOrder[0]
      // add blank page to end
      if ((this.data.templates.pages.left.end && lastPagePagination === 'right') ||
        (this.data.templates.pages.right.end && lastPagePagination === 'left')) {
        pages++
      }
      // update page layout
      if (pages !== this.pages) {
        this.pages = pages
        this.els.margins.innerHTML = ''
        let pagination = this.startPos

        let modelStart
        const dataClone = JSON.parse(JSON.stringify(this.data))
        for (let page = 1; page <= this.pages; page++) {
          modelStart = 'add_page_' + pagination
          const { element: pageEl } = this.els.margins.renderTpl(modelStart, this.data, 'app')
          pageEl.assignTPL(
            dataClone,
            window.M.NS.layout.themes,
            this.getPageNumberCounter(page),
            page,
            this.pages
          )
          pagination = pagination === 'left' ? 'right' : 'left'
        }
      }

      // scroll to caret
      let positionEl = this.els.body.querySelector('__caret__')
      if (!noMoveToCaret) {
        if (!positionEl) { // caret in attribute
          positionEl = this.els.body.querySelector('*[__caret]')
          if (!positionEl && data.position) { // no caret go to line
            positionEl = this.els.body.querySelector(`*[__line="${data.position.lineNumber}"]`)
          }
        }
        if (positionEl) {
          this._scrollToPage(this.getPageNumber(positionEl))
          // positionEl.scrollIntoView({ block: 'center' })
        } else {
          this._scrollToPage(1)
        }
      } else { // no caret in editor
        if (positionEl) {
          positionEl.remove()
        }
        this._scrollToPage(1)
      }
    }
  }
}
