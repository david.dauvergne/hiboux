function findLine (me, line) {
  const element = me.querySelector(`*[__line="${line + 1}"]`)
  if (element) {
    element.scrollIntoView({ block: 'center' })
  }
}

module.exports = {
  dom: {
    insert: function () {
      this.body = {
        type: 'tag',
        name: 'div',
        coulumnStart: [],
        attributes: {
          el: 'body'
        },
        children: []
      }
      this.vDom = JSON.parse(JSON.stringify(this.body))
      this.els.body.__$ns = 'reflow'
    }
  },
  events: {
    body: {
      click: function (e) {
        const range = document.caretRangeFromPoint(e.clientX, e.clientY)
        const textNode = range.startContainer
        const offset = range.startOffset - 1
        if (textNode && textNode.nodeType === 3) {
          let parentNode
          if (textNode.parentNode.getAttribute('el') === 'content') {
            parentNode = textNode.parentNode.__$refparent
          } else {
            parentNode = textNode.parentNode
          }
          const indexNode = Array.prototype.indexOf.call(textNode.parentNode.childNodes, textNode)
          const column = parentNode.coulumnStart[indexNode] + offset
          this.dispatch$('setPosition', {
            lineNumber: (parentNode.getAttribute('__line') * 1) + 1,
            column: column + 1
          })
        }
      }
    }
  },
  methods: {
    $p_displayMode: function (data) {
      if (data[0]) {
        this.style.display = data[0] === 'reflow' ? 'block' : 'none'
      }
    },
    $p_editorContent: function (data) {
      if (this.style.display === 'block' && data[0]) {
        if (data[0].content && data[0].content.children) {
          const body = JSON.parse(JSON.stringify(this.body))
          body.children = data[0].content.children

          const patch = window.M.diff(this.vDom, body)
          patch(this.els.body)
          this.vDom = body

          let caret = this.querySelector('__caret__')
          if (caret) {
            caret.scrollIntoView({ block: 'center' })
          } else {
            caret = this.querySelector('*[__caret]')
            if (caret) {
              const mode = caret.getAttribute('__caret')
              if (mode === 'self') {
                caret.scrollIntoView({ block: 'center' })
              } else {
                findLine(this, mode)
              }
            } else {
              if (data[0].position) {
                findLine(this, data[0].position.lineNumber)
              }
            }
          }
        }
      }
    }
  }
}
