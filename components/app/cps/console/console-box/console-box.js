module.exports = {
  dom: {
    create: function () {
      var me = this
      window.M.ipc.on('error', function (event, message) {
        if (message.clear) {
          me.innerHTML = ''
        }
        switch (message.type) {
          case 'comment':
          case 'title':
            me.renderTpl(message.type, { message: message.message })
            break
          case 'assert':
            me.renderTpl('assert', message)
            break
          case 'nothing':
            //
            break
          default:
            me.renderTpl('error', message.err)
        }
      })
    }
  }
}
