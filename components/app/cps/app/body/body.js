const prefSchema = require('./prefsSchema')
const { getAllPackages, checkVersion } = require('./lib/package-manager')

module.exports = {
  dom: {
    insert: function () {
      window.M.ipc.on('pdf-finish', (event, message) => {
        // console.log(message)
        this.els.waitPdf.close()
      })
      // this.els.editor.load('.mcl', '/foo/bar.mcl', `p❲Probably few of the phenomena ❬b❲foo❭ Natured even of awe.`)
      window.M.ipc.on('file-load', (event, message) => {
        if (message.file) {
          this.els.editor.load('.mcl', message.file, message.content)
        }
      })
    }
  },
  methods: {
    $dialogAlert: function (title, message) {
      const { element: dialog, els } = this.renderTpl('dialogAlert', { title: title, message: message })
      this.assignEvents(els.close, null, {
        click: function () {
          dialog.close()
          dialog.remove()
        }
      })
      dialog.showModal()
    },
    $dialogConfirm: function (title, message, cb) {
      const { element: dialog, els } = this.renderTpl('dialogConfirm', { title: title, message: message })
      this.assignEvents(dialog, els, {
        cancel: {
          click: function () {
            dialog.close()
            dialog.remove()
          }
        },
        confirm: {
          click: function () {
            dialog.close()
            dialog.remove()
            cb()
          }
        }
      })
      dialog.showModal()
    },
    $dialogConfiguration: function (render, close) {
      const { element: dialog, els } = this.renderTpl('dialogConfiguration')
      els.configuration.assignCloseFnc(() => {
        close(els.configuration)
        dialog.close()
        dialog.remove()
      })
      render(els.configuration)
      dialog.showModal()
    },
    $reloadApp: function () {
      document.head.querySelectorAll('*[type="plugin"]').forEach(function (plugin) {
        plugin.remove()
      })
      document.head.querySelectorAll('style[class="monaco-colors"] ~ style').forEach(function (style) {
        style.remove()
      })
      window.M.events.removeAllListeners()
    }
  },
  events: {
    preferences: {
      click: function () {
        this.dispatch$('dialogConfiguration', (confElement) => {
          const { els } = this.renderTpl('preferences', null, null, confElement)
          els.config.assign([], prefSchema)
          const ns = window.M.preferences.get('namespace')

          // installedPackages
          els.preLeftpan.querySelector('*[reftab="prefPackages"]')
            .addEventListener('click', () => {
              const { els: elsInstalledPackages } = this.renderTpl('installedPackages', null, null, els.prefPackages)
              for (const nsName in ns) {
                const { els: elsNs } = this.renderTpl('namespaceBox', { title: nsName }, null, elsInstalledPackages.namespaces)
                this.renderTpl({
                  position: 'beforeend',
                  content: [{
                    type: 'tag',
                    name: 'plugin-box',
                    attributes: {},
                    children: []
                  }]
                }, window.M.NS[nsName].packages, null, elsNs.container)
              }
            })

          // installPackages
          els.preLeftpan.querySelector('*[reftab="prefInstall"]')
            .addEventListener('click', () => {
              const { els: elsInstallPackages } = this.renderTpl('installPackages', null, null, els.prefInstall)
              const packages = getAllPackages()
              packages.then(data => {
                const nsCreate = {}
                data.forEach((plugin) => {
                  const namespace = plugin.namespace
                  const name = plugin.name
                  if (ns[namespace]) {
                    if (!ns[namespace].packages[name]) {
                      if (!nsCreate[namespace]) {
                        const { els: elsNs } = this.renderTpl('namespaceBox', { title: namespace }, null, elsInstallPackages.namespaces)
                        nsCreate[namespace] = elsNs.container
                      }
                      this.renderTpl({
                        position: 'beforeend',
                        content: [{
                          type: 'tag',
                          name: 'plugin-install',
                          attributes: {},
                          children: []
                        }]
                      }, plugin, null, nsCreate[namespace])
                    }
                  }
                })
              }).catch(err => {
                console.linter.parse(err, {
                  file: 'app://app/body.js',
                  ext: '.js'
                })
              })
            })
          // updatePackages
          els.preLeftpan.querySelector('*[reftab="prefUpdates"]')
            .addEventListener('click', () => {
              const { els: elsUpdatePackages } = this.renderTpl('updatePackages', null, null, els.prefUpdates)
              const promiseArr = []
              for (const nsName in ns) {
                window.M.NS[nsName].packages.forEach((item) => {
                  if (!item.core) {
                    promiseArr.push(
                      new Promise((resolve, reject) => {
                        checkVersion(item.name, item.version, (isUpdate) => {
                          resolve({ package: item, isUpdate: isUpdate })
                        })
                      })
                    )
                  }
                })
              }
              Promise.all(promiseArr).then(values => {
                console.log(values)
              }, reason => {
                console.log(reason)
              })

            })
        }, (confElement) => {
          document.body.trigger('reloadApp')
        })
      }
    },
    pdfMaker: {
      click: function () {
        this.els.waitPdf.showModal()
        window.M.ipc.send('createPdf', {
          contents: {
            [this.els.editor.p_editorContent.uri]: {
              content: this.els.editor.p_editorContent.content
            }
          },
          models: this.els.layout.models
        })
      }
    },
    display: {
      click: function () {
        this.p_displayMode = this.p_displayMode === 'layout' ? 'reflow' : 'layout'
        this.els.editor.changeDisplay()
      }
    },
    openfile: {
      click: function () {
        window.M.ipc.send('openfile')
      }
    }
  },
  properties: {
    // a global property starts with the $ character
    $p_displayMode: 'layout'
  }
}
