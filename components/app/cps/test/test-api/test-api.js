module.exports = {
  modules: ['test'],
  dom: {
    create: function () {
      console.test.title('API Console test')
      console.test.assert('assert (OK)', 'OK', 'OK')
      console.test.assert('assert (KO)', 'OK', 'KO')

      console.test.title('API dom')
      console.test.assert('Dom (create)', 'OK', 'OK')
    },
    insert: function () {
      console.test.assert('Dom (insert)', 'OK', 'OK')
      this.els.propTest.addEventListener('click', this.tryFunction('qsdf', function (e) {
        e.stopPropagation()
        this.errror()
      }))
    }
  },
  methods: {
    foo: function () {
      return 'OK'
    }
  },
  events: {
    bar: {
      mousedown: function (e) {
        e.stopPropagation()
        console.log('mousedown', this)
      },
      mouseup: function (e) {
        e.stopPropagation()
        console.log('mouseup')
      }
    },
    mouseup: function (e) {
      e.preventDefault()
      M.preferences.set('bar.foo', 678)

      console.test.title('API methods')
      console.test.assert('Method', 'OK', this.foo())

      console.test.title('API Modules')
      console.test.assert('Modules test (M.test)', 'OK', M.test.ok())

      var bodyEl = document.body

      console.test.title('API get element from property "els"')
      console.test.assert('<body> get element (bodyEl.els.foo)', 'true', (typeof bodyEl.els.foo === 'object'))
      console.test.assert('<body> get element (bodyEl.els.bar)', 'false', (typeof bodyEl.els.bar === 'object'))
      console.test.assert('<test-api> get element (this.els.bar)', 'true', (typeof this.els.bar === 'object'))
      console.test.assert('<test-api> get element (this.els.content)', 'true', (typeof this.els.content === 'object'))
      console.test.assert('<test-api> get element (this.els.xxx)', 'false', (typeof this.els.xxx === 'object'))

      console.test.title('API element (el="content")')
      console.test.assert('<test-api> get in element content child[0]', 'BODY ', (this.els.content.childNodes[0].textContent))
      console.test.assert('<test-api> get in element content child[1]', 'FOO', (this.els.content.childNodes[1].textContent))
      console.test.assert('<test-api> get all in element content', 'BODY FOO', this.els.content.textContent)

      console.test.title('API properties')
      console.test.assert('Property get', 'AAA', this.propTest)
      console.test.assert('Property get from element', 'AAA', this.els.propTest.textContent)
      console.test.comment('Property assign new value')
      this.propTest = 'BBB'
      console.test.assert('Property get', 'BBB', this.propTest)
      console.test.assert('Property get from element', 'BBB', this.els.propTest.textContent)

      console.test.title('API attributes')
      console.test.assert('Attribute get (this.attrs.x)', 'AAA', this.attrs.att)
      console.test.assert('Attribute get (this.getAttribute.("x"))', 'AAA', this.getAttribute('att'))
      console.test.assert('Attribute get from element', 'AAA', this.els.attrTest.textContent)
      console.test.comment('Attribute assign new value (this.attrs.x)')
      this.attrs.att = 'BBB'
      console.test.assert('Attribute get (this.attrs.x)', 'BBB', this.attrs.att)
      console.test.assert('Attribute get (this.getAttribute.("x"))', 'BBB', this.getAttribute('att'))
      console.test.assert('Attribute get from element', 'BBB', this.els.attrTest.textContent)
      console.test.comment('Attribute assign new value (this.setAttribute.("x", value))')
      this.setAttribute('att', 'CCC')
      console.test.assert('Attribute get (this.attrs.x)', 'CCC', this.attrs.att)
      console.test.assert('Attribute get (this.getAttribute.("x"))', 'CCC', this.getAttribute('att'))
      console.test.assert('Attribute get from element', 'CCC', this.els.attrTest.textContent)

      console.test.title('API renderTpl')
      var lang = M.preferences.get('lang')
      var tpllocalEls = this.renderTpl('tpl_local')
      console.test.comment('Assign locales')
      console.test.assert('Locale {$localeName})', 'XX (' + lang + ')', tpllocalEls.els.xx.textContent)
      console.test.assert('Locale {$sub.localeName})', 'YY (' + lang + ')', tpllocalEls.els.subyy.textContent)
      console.test.assert('Locale attribute (attr="{$localeName}")', 'ATTR (' + lang + ')', tpllocalEls.els.attrTest.getAttribute('title'))

      console.test.comment('Assign datas')
      var tplDataEls = this.renderTpl('tpl_data', { attr: 'ATTR', xx: 'XX', sub: { yy: 'YY' } })
      console.test.assert('["pl_data"] Element reference property "xx"', 'true', (typeof tplDataEls.els.xx === 'object'))
      console.test.assert('["pl_data"] Data [$dataName])', 'XX', tplDataEls.els.xx.textContent)
      console.test.assert('["pl_data"] Data [$sub.dataName])', 'YY', tplDataEls.els.subyy.textContent)
      console.test.assert('["pl_data"] Data attribute (attr="[$dataName]")', 'ATTR', tplDataEls.els.attrTest.getAttribute('title'))

      console.test.comment('Assign multi-datas')
      var w = this.renderTpl('tpl_multidata', [
        { el: 'xxx', val: 'XX' },
        { el: 'yyy', val: 'YY' }
      ])
      console.test.assert('["tpl_multidata"] Element reference property "xxx"', 'true', (typeof w.els.xxx === 'object'))
      console.test.assert('["tpl_multidata"] Data [$dataName])', 'XX', w.els.xxx.textContent)
      console.test.assert('["tpl_multidata"] Element reference property "yyy"', 'true', (typeof w.els.yyy === 'object'))
      console.test.assert('["tpl_multidata"] Data [$dataName])', 'YY', w.els.yyy.textContent)

      console.test.title('API renderTpl object AST')
      var tplObjectEls = this.renderTpl({
        overlay: 'body',
        position: 'beforeend',
        content: [{
          type: 'tag',
          name: 'p',
          attributes: {
            el: 'xxx',
            title: 'ATTR'
          },
          children: [{ type: 'text', data: 'XX' }]
        }]
      })
      console.test.assert('[renderTpl AST] Element reference property "xxx"', 'true', (typeof tplObjectEls.els.xxx === 'object'))
      console.test.assert('[renderTpl AST] Get textContent', 'XX', tplObjectEls.els.xxx.textContent)
      console.test.assert('[renderTpl AST] Data attribute (attr="[$dataName]")', 'ATTR', tplObjectEls.els.xxx.getAttribute('title'))

      console.test.comment('API renderTpl object AST and data')
      var tplObjectDataEls = this.renderTpl({
        overlay: 'body',
        position: 'beforeend',
        content: [{
          type: 'tag',
          name: 'html:p',
          attributes: {
            el: 'xxx',
            title: '$[attr]'
          },
          children: [{ type: 'text', data: '$[xx]' }]
        }]
      }, { attr: 'ATTR', xx: 'XX' })
      console.test.assert('[renderTpl AST] Element reference property "xxx"', 'true', (typeof tplObjectDataEls.els.xxx === 'object'))
      console.test.assert('[renderTpl AST] Get textContent', 'XX', tplObjectDataEls.els.xxx.textContent)
      console.test.assert('[renderTpl AST] Data attribute (attr="[$dataName]")', 'ATTR', tplObjectDataEls.els.xxx.getAttribute('title'))

      console.test.title('API react')
      console.test.comment('Create product elements')
      this.renderTpl('app-react')

      console.test.title('API error for method "renderTpl"')
      console.test.comment('ReferenceError: template "xxxxxx" does not exist')
      this.renderTpl('xxxxxx')
      console.test.comment('ReferenceError: overlay "xxxxxx" does not exist in document')
      this.renderTpl({
        overlay: 'xxxxxx',
        position: 'beforeend',
        content: [{ type: 'tag', name: 'h1', children: [] }]
      })
      console.test.comment('SyntaxError: invalid overlay template definition')
      this.renderTpl('tpl_error_overlay')
      this.renderTpl({
        position: 'beforeend',
        content: [{ type: 'tag', name: 'h1', children: [] }]
      })
      console.test.comment('RangeError: invalid position template definition [beforebegin, afterbegin, beforeend, afterend]')
      this.renderTpl('tpl_error_position')
      this.renderTpl({
        overlay: 'body',
        position: 'xxxxxx',
        content: [{ type: 'tag', name: 'h1', children: [] }]
      })
      console.test.comment('SyntaxError: invalid template')
      this.renderTpl()
      this.renderTpl({
        overlay: 'body',
        position: 'beforeend'
      })
    }
  },
  properties: {
    propTest: 'AAA'
  }
}
