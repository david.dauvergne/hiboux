
module.exports = {
  dom: {
    remove: function () {
      console.log('Dom remove, quantity:', this.quantity)
    }
  },
  // Events
  events: {
    add: {
      click: function (e) {
        e.stopPropagation()
        this.quantity++
      }
    },
    del: {
      click: function (e) {
        e.stopPropagation()
        if (this.quantity > 0) {
          this.quantity--
        }
      }
    },
    rm: {
      click: function (e) {
        e.stopPropagation()
        this.remove()
      }
    }
  },

  // definition of properties
  properties: {
    // a global property starts with the $ character
    $quantity: 0
  }
}
