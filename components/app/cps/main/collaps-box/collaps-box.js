module.exports = {
  events: {
    collaps: {
      click: function (e) {
        e.stopPropagation()
        if (this.attrs.collapsed === 'true') {
          this.attrs.collapsed = 'false'
        } else {
          this.attrs.collapsed = 'true'
        }
      }
    }
  },
  attributes: {
    title: {
      set: function (value) {
        this.els.title.innerHTML = value
      }
    },
    collapsed: {
      set: function (value) {
        if (value === 'true') {
          this.els.content.style.display = 'none'
        } else {
          this.els.content.style.display = 'block'
        }
      }
    }
  }
}
