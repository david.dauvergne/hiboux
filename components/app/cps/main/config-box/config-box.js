module.exports = {
  events: {
    close: {
      click: function () {
        if (this.closeFnc) {
          this.closeFnc()
          this.closeFnc = null
          this.els.content.innerHTML = ''
        }
      }
    }
  },
  methods: {
    assignCloseFnc: function (closeFnc) {
      this.closeFnc = closeFnc
    }
  }
}
