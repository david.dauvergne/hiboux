module.exports = {
  dom: {
    insert: function () {
      const validsPan = ['leftpan', 'rightpan', 'footpan']
      this.querySelectorAll(':scope > flex-box[el]').forEach(function (flexBox) {
        const elName = flexBox.attrs.el
        if (validsPan.includes(elName)) {
          const keyPath = `flexBox.${elName}`
          if (M.preferences.has(keyPath)) {
            flexBox.style.flex = `0 0 ${M.preferences.get(keyPath)}px`
          }
        }
      })
    }
  },
  methods: {
    updateFlexBox: function (box) {
      if (box.prev.el.__$name === 'flex-box') {
        const keyPath = `flexBox.${box.prev.el.getAttribute('el')}`
        M.preferences.set(keyPath, box.prev.size)
      }
      if (box.next.el.__$name === 'flex-box') {
        const keyPath = `flexBox.${box.next.el.getAttribute('el')}`
        M.preferences.set(keyPath, box.next.size)
      }
    }
  }
}
