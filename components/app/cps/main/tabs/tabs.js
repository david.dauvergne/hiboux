module.exports = {
  dom: {
    insert: function () {
      this.tabs = []
      const buttons = this.querySelectorAll('*[reftab]')
      buttons.forEach((button) => {
        const elName = button.getAttribute('reftab')
        const pan = this.querySelector(`*[el="${elName}"]`)
        if (pan) {
          this.tabs[elName] = { pan: pan, button: button }
          this.assignEvents(button, null, {
            click: () => {
              this.toogle(elName)
            }
          })
        }
      })
    }
  },
  methods: {
    toogle: function (elName, button) {
      for (const pan in this.tabs) {
        const tab = this.tabs[pan]
        if (elName === pan) {
          tab.pan.style.display = 'block'
          tab.button.setAttribute('selected', 'true')
        } else {
          tab.pan.style.display = 'none'
          tab.button.removeAttribute('selected')
        }
      }
    }
  }
}
