module.exports = {
  dom: {
    insert: function () {
      this.__offset = 'width'
      this.__dir = 'left'
      this.__client = 'clientX'
      if (this.attrs.type === 'horizontal') {
        this.__offset = 'height'
        this.__dir = 'top'
        this.__client = 'clientY'
      }
      this.__gutterSize = this.getBoundingClientRect()[this.__offset] / 2
      this.position = this.attrs.dock.split('-')[0]
      this.__oldPos = {
        prevSize: 80,
        nextSize: 80
      }
    }
  },
  methods: {
    $windowResize: function () {
      // const prevEl = this.previousElementSibling
      // const nextEl = this.nextElementSibling
      // const prevSize = prevEl.getBoundingClientRect()[this.__offset]
      // const nextSize = nextEl.getBoundingClientRect()[this.__offset]
      // this.dispatch$('flexSplitterMove', {
      //   prev: { el: prevEl, size: prevSize },
      //   next: { el: nextEl, size: nextSize }
      // })
    },
    _move: function (ev) {
      const value = ev[this.__client] - this.__gutterSize
      if (value < this.__max && value > this.__min) {
        this[this.__dir] = value
        this.els.ghost.style[this.__dir] = value + 'px'
      }
    }
  },
  events: {
    dock: {
      mouseup: function (e) {
        e.stopPropagation()
      },
      mousedown: function (e) {
        e.stopPropagation()
        const prevEl = this.previousElementSibling
        const nextEl = this.nextElementSibling
        const prevSize = prevEl.getBoundingClientRect()[this.__offset]
        const nextSize = nextEl.getBoundingClientRect()[this.__offset]
        let prevNewSize, nextNewSize
        if (this.position === 'left' || this.position === 'top') {
          if (prevSize === 0) {
            prevNewSize = this.__oldPos.prevSize
            nextNewSize = nextSize - this.__oldPos.prevSize
            prevEl.style.flex = `0 0 ${this.__oldPos.prevSize}px`
            nextEl.style.flex = `0 0 ${nextSize - this.__oldPos.prevSize}px`
          } else {
            this.__oldPos = { prevSize: prevSize }
            prevNewSize = 0
            nextNewSize = nextSize + prevSize
            prevEl.style.flex = '0 0 0px'
            nextEl.style.flex = `0 0 ${nextNewSize}px`
          }
        } else if (this.position === 'right' || this.position === 'bottom') {
          if (nextSize === 0) {
            prevNewSize = prevSize - this.__oldPos.nextSize
            nextNewSize = this.__oldPos.nextSize
            prevEl.style.flex = `0 0 ${prevSize - this.__oldPos.nextSize}px`
            nextEl.style.flex = `0 0 ${this.__oldPos.nextSize}px`
          } else {
            this.__oldPos = { nextSize: nextSize }
            prevNewSize = nextSize + prevSize
            nextNewSize = 0
            nextEl.style.flex = '0 0 0px'
            prevEl.style.flex = `0 0 ${prevNewSize}px`
          }
        }
        const elsSize = {
          prev: { el: prevEl, size: prevNewSize },
          next: { el: nextEl, size: nextNewSize }
        }
        this.parentNode.updateFlexBox(elsSize)
        this.dispatch$('flexSplitterMove', elsSize)
      }
    },
    mousedown: function () {
      this.prevEl = this.previousElementSibling
      this.nextEl = this.nextElementSibling
      var rect = this.getBoundingClientRect()
      this.prevRect = this.prevEl.getBoundingClientRect()
      this.nextRect = this.nextEl.getBoundingClientRect()
      this.prevSize = this.prevRect[this.__offset]
      this.nextSize = this.nextRect[this.__offset]
      this.__max = this.prevSize + this.nextSize + this.prevRect[this.__dir]
      this.__min = this.prevRect[this.__dir]
      this.els.ghost.style.display = 'block'
      this.els.ghost.style.height = rect.height + 'px'
      this.els.ghost.style.width = rect.width + 'px'
      if (this.__dir === 'left') {
        this.els.ghost.style.top = rect.top + 'px'
      }
      this.prevEl.style.webkitUserSelect = 'none'
      this.nextEl.style.webkitUserSelect = 'none'
      window.addEventListener('mousemove', this._move)
    },
    mouseup: function () {
      this.els.ghost.style.display = 'none'
      this.prevEl.style.webkitUserSelect = ''
      this.nextEl.style.webkitUserSelect = ''
      try {
        window.removeEventListener('mousemove', this._move)
        var globalSize = (this.nextSize + this.prevSize)
        var prevNewSize = this[this.__dir] - this.prevRect[this.__dir]
        this.prevEl.style.flex = `0 0 ${prevNewSize}px`
        var nextNewSize = globalSize - prevNewSize
        this.nextEl.style.flex = `0 0 ${nextNewSize}px`
        const elsSize = {
          prev: { el: this.prevEl, size: prevNewSize },
          next: { el: this.nextEl, size: nextNewSize }
        }
        this.parentNode.updateFlexBox(elsSize)
        this.dispatch$('flexSplitterMove', elsSize)
      } catch (err) {}
    }
  }
}
