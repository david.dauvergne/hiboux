module.exports = {
  dom: {

  },
  methods: {
    toEnum: function (pref, obj, container) {
      const { els } = this.renderTpl('enum', obj, null, container)
      this.assignEvents(els.selectlist, null, {
        change: function () {
          window.M.preferences.set(obj.prefkey, this.value)
        }
      })
      obj.enum.forEach((value) => {
        let tpl = 'enumitem'
        if (pref === value) {
          tpl = 'enumitemSelected'
        }
        this.renderTpl(tpl, { value: value }, null, els.selectlist)
      })
    },
    toInput: function (pref, obj, container, mode) {
      const { els } = this.renderTpl(mode, obj, null, container)
      if (pref !== obj.default) {
        els.input.value = pref
      }
      this.assignEvents(els.input, null, {
        change: function () {
          window.M.preferences.set(obj.prefkey, this.value)
        }
      })
    },
    assign: function (parentPath, schema, container) {
      if (!container) {
        container = this
      }
      for (const key in schema) {
        const obj = schema[key]
        const selfPath = parentPath.concat(key)
        if (obj.type) {
          obj.prefkey = selfPath.join('.')
          const pref = window.M.preferences.get(obj.prefkey)
          switch (obj.type) {
            case 'object':
              var { element: collapsBox, els } = this.renderTpl('object', obj, null, container)
              if (obj.collapsed) {
                collapsBox.setAttribute('collapsed', 'true')
              } else {
                collapsBox.setAttribute('collapsed', 'false')
              }
              this.assign(selfPath, obj.properties, els.container)
              break
            case 'string':
            case 'number':
            case 'integer':
              if (obj.enum) {
                this.toEnum(pref, obj, container)
              } else {
                this.toInput(pref, obj, container, obj.type)
              }
              break
            case 'boolean':
              ({ els } = this.renderTpl('boolean', obj, null, container))
              if (pref === true) {
                els.input.checked = true
              }
              this.assignEvents(els.input, null, {
                change: function () {
                  window.M.preferences.set(obj.prefkey, this.checked)
                }
              })
              break
          }
        } else {
          console.log(obj);
          console.log('errrr')
        }
      }
    }
  }
}
