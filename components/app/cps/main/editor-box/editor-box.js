const linterMcl = require('../lib/linterMcl')
const fileTypes = {
  '.css': 'css',
  '.js': 'javascript',
  '.json': 'json',
  '.md': 'markdown',
  '.mcl': 'mcl'
}
const whitespaceReg = /([ ]{2,})/g
const arrowCode = ['ArrowRight', 'ArrowLeft', 'ArrowDown', 'ArrowUp']

const backspace = function backspace (editor, selection) {
  editor.executeEdits('', [{
    range: {
      startLineNumber: selection.startLineNumber,
      startColumn: selection.startColumn - 1,
      endLineNumber: selection.startLineNumber,
      endColumn: selection.startColumn
    },
    text: '',
    forceMoveMarkers: true
  }])
}

module.exports = {
  properties: {
    // a global property starts with the $ character
    $p_editorContent: null
  },
  methods: {
    $reloadApp: function () {
      if (this.model) {
        this.model.dispose()
        this.p_editorContent = null
      }
    },
    $setPosition: function (position) {
      if (this.model && this.model._languageIdentifier.language === 'mcl' && this.p_editorContent) {
        this.editor.focus()
        this.editor.setPosition(position)
        this.editor.revealPosition(position)
        this.updateView(position)
      }
    },
    $setRange: function (position) {
      if (this.model && this.model._languageIdentifier.language === 'mcl' && this.p_editorContent) {
        this.editor.focus()
        const range = new monaco.Range(
          position.anchorLineNumber,
          position.anchorColumn,
          position.focusLineNumber,
          position.focusColumn
        )
        this.editor.setSelection(range)
        this.editor.revealRange(range)
        // this.updateView(position)
      }
    },
    $flexSplitterMove: function (box) {
      if (box.prev.el.__$name === 'flex-container') {
        this.style.width = box.prev.size + 'px'
      }
      if (box.next.el.__$name === 'flex-container') {
        this.style.width = box.next.size + 'px'
      }
    },
    changeDisplay: function () {
      if (this.model && this.model._languageIdentifier.language === 'mcl' && this.p_editorContent) {
        this.updateContent(this.p_editorContent.content, this.p_editorContent.position, true)
      }
    },
    updateContent: function (content, position, addStart) {
      if (!addStart) {
        content.children.unshift({ type: 'tag', name: '__start__', coulumnStart: [], attributes: {}, children: [] })
        content.children.push({ type: 'tag', name: '__last__', coulumnStart: [], attributes: {}, children: [] })
      }
      this.p_editorContent = {
        content: content,
        position: position,
        uri: this.uri
      }
    },
    updateView: function (position) {
      let value = this.model.getValue()
      // caret insert
      const indexCaret = this.model.getOffsetAt(position)
      value = value.substring(0, indexCaret) + '❚' + value.substring(indexCaret)
      // linter mcl
      console.linter.clear()
      const data = linterMcl(this.editor.uri, value)
      if (data.error.length === 0) {
        this.updateContent(data.root, position)
      }
    },
    _changeContent: function () {
      let value = this.model.getValue()
      const position = this.editor.getPosition()
      // caret insert
      const indexCaret = this.model.getOffsetAt(position)
      value = value.substring(0, indexCaret) + '❚' + value.substring(indexCaret)
      // linter mcl
      console.linter.clear()
      const data = linterMcl(this.editor.uri, value)
      // decorations
      const newDecorations = []
      if (data.error.length > 0) {
        data.error.forEach((err) => {
          if (err.severity === 'error') {
            newDecorations.push({
              range: new monaco.Range(err.lineStart, err.coulumnStart + 1, err.lineEnd, err.coulumnEnd + 1),
              options: {
                glyphMarginClassName: 'monaco-mcl-errorIcon',
                glyphMarginHoverMessage: { value: err.excerpt },
                className: 'squiggly-error'
              }
            })
          }
        })
      } else {
        // no errors
        this.updateContent(data.root, position)

        // lines empties & multi-spaces
        data.lines.forEach((line) => {
          newDecorations.push({
            range: new monaco.Range(line + 1, 1, line + 1, 1),
            options: {
              isWholeLine: true,
              className: 'monaco-mcl-emptyLine'
            }
          })
        })
        let match
        while ((match = whitespaceReg.exec(value))) {
          const startPos = this.model.getPositionAt(match.index)
          const endPos = this.model.getPositionAt(match.index + match[0].length)
          newDecorations.push({
            range: new monaco.Range(startPos.lineNumber, startPos.column, endPos.lineNumber, endPos.column - 1),
            options: {
              className: 'monaco-mcl-multispace'
            }
          })
        }
      }
      this.model.decorations = this.editor.deltaDecorations(this.model.decorations, newDecorations)
      this.tolint = true
    },
    load: function (ext, file, content) {
      if (this.model) {
        this.model.dispose()
        this.p_editorContent = null
      }
      this.tolint = true
      const timeRender = () => {
        setTimeout(() => {
          if (this.tolint) {
            this.tolint = false
            this._changeContent()
          }
        }, 50)
      }
      M.monaco((monaco) => {
        this.uri = file
        this.model = monaco.editor.createModel('', fileTypes[ext], this.uri)
        this.model.decorations = []
        this.editor.setModel(this.model)
        if (ext === '.mcl') {
          this.model.onDidChangeContent((e) => {
            timeRender()
          })
        }
        this.editor.setValue(content)
      })
    }
  },
  dom: {
    insert: function () {
      M.monaco((monaco) => {
        const c = window.M.NS.app.themes.jsoncss
        monaco.editor.defineTheme('mclTheme', {
          base: 'vs-dark',
          inherit: true,
          rules: [
            { token: 'nobreakspace', foreground: c['--color-editor-nobreakspace'], fontStyle: 'underline' },
            { token: 'comment', foreground: c['--color-editor-comment'], fontStyle: 'italic' },
            { token: 'tag-name', foreground: c['--color-editor-tag-name'] },
            { token: 'tag-bracket', foreground: c['--color-editor-tag-bracket'] },
            { token: 'attr-bracket', foreground: c['--color-editor-attr-bracket'] },
            { token: 'attr-key', foreground: c['--color-editor-attr-key'] },
            { token: 'attr-equal', foreground: c['--color-editor-attr-equal'] },
            { token: 'attr-value', foreground: c['--color-editor-attr-value'] },
            { token: 'attr-separator', foreground: c['--color-editor-attr-separator'] },
            { token: 'cdata-bracket', foreground: c['--color-editor-cdata-bracket'] },
            { token: 'cdata-text', foreground: c['--color-editor-cdata-text'], fontStyle: 'italic' }
          ],
          colors: {
            'editor.lineHighlightBackground': c['--color-editor-lineHighlightBackground'],
            'editorCursor.foreground': c['--color-editor-cursor.foreground']
          }
        })
        this.editor = monaco.editor.create(this, {
          automaticLayout: true,
          // fontFamily: 'DejaVuSansMono',
          fontSize: '14px',
          theme: 'mclTheme',
          wordWrap: 'on',
          wordSeparators: '❬❭❲❘➔❳❨❩❛❜ /\\()"\':,.;<>~!@#%^&*|+=[]{}`?-',
          glyphMargin: true,
          // wordWrapColumn: 75,
          // rulers: [75],
          tabSize: 2,
          wordWrapMinified: true,
          wrappingIndent: 'indent',
          // selectionHighlight: false,
          occurrencesHighlight: false,
          insertSpaces: false
          // lineNumbers: 'off'
        })

        // MCl event
        // this.editor.onDidChangeCursorPosition((e) => {
        //   // console.log(e);
        // })
        this.editor.onKeyUp((e) => {
          this.keyCode = e.code
          if (!e.ctrlKey && !e.shiftKey && !e.altKey && !e.ctrlKey && !e.metaKey) {
            if (arrowCode.includes(e.code)) {
              this.updateView(this.editor.getPosition())
            }
          }
        })
        this.editor.onMouseUp((e) => {
          if (this.model && this.model._languageIdentifier.language === 'mcl') {
            if (e.target.type === 6 || e.target.type === 7) { // inner editor
              const position = this.editor.getPosition()
              this.updateView(position)
            }
          }
        })
        const contribution = this.editor.getContribution('snippetController2')
        this.editor.addCommand(monaco.KeyMod.CtrlCmd | monaco.KeyCode.Space, () => {
          if (this.model && this.model._languageIdentifier.language === 'mcl') {
            const selection = this.editor.getSelection()
            const text = this.model.getValueInRange(selection)
            if (!text) {
              const lineContent = this.model.getLineContent(selection.startLineNumber)
              const leftChar = lineContent[selection.startColumn - 2]
              const leftLeftChar = lineContent[selection.startColumn - 3]
              const rightChar = lineContent[selection.startColumn - 1]
              const lineBeforeCursor = lineContent.substring(0, selection.startColumn - 1)

              if (lineContent.match(/^\s*!/)) {
                backspace(this.editor, selection)
                contribution.insert('❗ ${0}')
              } else if (lineContent.match(/^\s*@$/)) {
                backspace(this.editor, selection)
                contribution.insert('✈❲${0}❳')
              } else if (leftChar === '❘') {
                contribution.insert('${1:k}➔${0:v}❘')
              } else if (rightChar === '❘') {
                contribution.insert('❘${1:k}➔${0:v}')
              } else if (leftChar === '❲') {
                if (rightChar === '❳') {
                  contribution.insert('${1:k}➔${0:v}')
                } else {
                  contribution.insert('${1:k}➔${0:v}❳')
                }
              } else if (rightChar === '❳') {
                contribution.insert('❘${1:k}➔${0:v}')
              } else if (leftChar === '(') {
                if (leftLeftChar === '❲(' || leftLeftChar === '❳(') {
                  backspace(this.editor, selection)
                  contribution.insert('❨${0}❩')
                } else {
                  contribution.insert(' ')
                }
              } else if (rightChar === '❭' && leftChar !== '❲' && leftChar !== '❳' && leftChar !== '❬' && leftChar !== '<') {
                contribution.insert('❲')
              } else {
                if (lineBeforeCursor.match(/^\t*((?:([a-zA-Z0-9]+)(:))?([-_a-zA-Z0-9:]+))$/) && rightChar !== '❲') {
                  contribution.insert('❲')
                } else if (leftChar === '<') {
                  backspace(this.editor, selection)
                  contribution.insert('❬${0}❭')
                } else {
                  contribution.insert(' ') // no break space
                }
              }
            }
          }
        })
      })
    }
  }
}
