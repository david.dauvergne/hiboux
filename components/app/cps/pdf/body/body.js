module.exports = {
  methods: {
    render: function (remote) {
      const data = remote.getData()
      for (const uri in data.contents) {
        const { element } = this.renderTpl('layoutPages', data.models[uri])
        document.body.style.width = data.models[uri].page.width + data.models[uri].unit
        document.body.style.height = data.models[uri].page.height + data.models[uri].unit
        element.render(data.contents[uri], true)
        const pageNumber = element.pages
        const pageWitdh = data.models[uri].page.width * 1000
        const pageHeight = data.models[uri].page.height * 1000
        const pdfOptions = {
          pageSize: {
            height: pageHeight,
            width: pageWitdh
          },
          marginsType: 1,
          printBackground: true
        }
        const next = function (page) {
          if (page <= pageNumber) {
            remote.printToPDF(pdfOptions, page, pageNumber, function () {
              page++
              element._scrollToPage(page)
              next(page)
            })
          } else {
            remote.finish()
          }
        }
        setTimeout(function () {
          next(1)
        }, 2000)
      }
    }
  }
}
