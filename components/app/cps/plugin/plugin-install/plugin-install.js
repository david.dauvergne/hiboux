const { installPackage } = require('./lib/package-manager')

module.exports = {
  events: {
    install: {
      click: function () {
        let cpsOrTheme = 'cps'
        if (this.data.theme) {
          cpsOrTheme = 'theme'
        }
        this.renderTpl({
          position: 'replace',
          content: [{
            type: 'tag',
            name: 'spinner',
            attributes: {
              style: 'border: 2px solid var(--color-very-dark-teal);border-top-color: var(--color-light-teal);'
            },
            children: []
          }]
        }, null, null, this.els.install)

        installPackage(
          this.data.name,
          this.data.namespace,
          cpsOrTheme,
          () => {
            this.remove()
          }
        )
      }
    }
  }
}
