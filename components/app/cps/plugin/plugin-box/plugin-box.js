const { removePackage } = require('./lib/package-manager')

module.exports = {
  dom: {
    insert: function () {
      let remove = true
      if (window.M.NS[this.data.namespace].config &&
        window.M.NS[this.data.namespace].config[this.data.name]) {
        this.els.settings.style.display = 'inline-block'
        remove = false
      }
      if (!this.data.core) {
        this.els.uninstall.style.display = 'inline-block'
        this.els.disable.style.display = 'inline-block'
        remove = false
      }
      if (this.data.disabledCore) {
        this.els.disable.style.display = 'inline-block'
        remove = false
      }
      if (remove) {
        this.remove()
      } else {
        const disabledPackages = window.M.preferences.get(`namespace.${this.data.namespace}.disabledPackages`)
        if (disabledPackages.includes(this.data.name)) {
          this.els.disable.style.display = 'none'
          this.els.enable.style.display = 'inline-block'
        }
      }
    }
  },
  events: {
    settings: {
      click: function () {
        const { element: dialog, els } = this.renderTpl('settings', null)
        const parentPath = ['namespace', this.data.namespace, 'packages', this.data.name]
        els.config.assign(parentPath, window.M.NS[this.data.namespace].config[this.data.name])
        this.assignEvents(els.close, null, {
          click: function () {
            dialog.close()
            dialog.remove()
          }
        })
        dialog.showModal()
      }
    },
    uninstall: {
      click: function () {
        this.dispatch$('dialogConfirm', this.locale.uninstall, this.locale.confirmUninstall + this.data.name, () => {
          window.M.preferences.delete(`namespace.${this.data.namespace}.packages.${this.data.name}`)
          delete window.M.NS[this.data.namespace].config[this.data.name]
          removePackage(this.data.name, this.data.namespace, 'cps', () => {
            this.remove()
          })
        })
      }
    },
    disable: {
      click: function () {
        window.M.preferences.set(`namespace.${this.data.namespace}.disabledPackages`, this.data.name)
        this.els.disable.style.display = 'none'
        this.els.enable.style.display = 'inline-block'
      }
    },
    enable: {
      click: function () {
        window.M.preferences.delete(`namespace.${this.data.namespace}.disabledPackages`, this.data.name)
        this.els.disable.style.display = 'inline-block'
        this.els.enable.style.display = 'none'
      }
    }
  }
}
