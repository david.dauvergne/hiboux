const { app, Menu, BrowserWindow, protocol, dialog, ipcMain } = require('electron')
const path = require('path')
const fs = require('fs')
const PDFDocument = require('pdf-lib').PDFDocument

let mainWindow

process.env.APP_DIR_COMPONENTS = path.join(__dirname, 'components')
process.env.APP_DIR_MODULES = path.join(__dirname, 'modules')
process.env.APP_USER_DATA = app.getPath('userData')
process.env.APP_REGISTRY_PACKAGES = 'http://localhost:4873'

const createMainWindow = function () {
  protocol.registerFileProtocol('app', (request, callback) => {
    const url = { path: path.normalize(`${process.env.APP_DIR_COMPONENTS}/${request.url.substr(6)}`) }
    callback(url)
  })

  protocol.registerFileProtocol('hiboux', (request, callback) => {
    const url = { path: path.normalize(`${app.getPath('userData')}/${request.url.substr(6)}`) }
    callback(url)
  })

  mainWindow = new BrowserWindow({
    webPreferences: {
      contextIsolation: false,
      experimentalFeatures: true,
      nodeIntegration: true
    }
  })
  mainWindow.webContents.WINDOW_ID = 'start'
  mainWindow.setMenu(null)
  mainWindow.maximize()
  mainWindow.loadURL(`file://${__dirname}/start.xhtml`)
  mainWindow.webContents.openDevTools()
  mainWindow.on('closed', function () {
    mainWindow = null
    app.quit()
  })
}

app.allowRendererProcessReuse = false

app.on('ready', createMainWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createMainWindow()
  }
})

app.on('browser-window-created', function () {
  Menu.setApplicationMenu(null)
})

ipcMain.on('allWindow', function (event, message) {
  message.WINDOW_ID = event.sender.webContents.WINDOW_ID
  mainWindow.webContents.send(message.event, message)
})

ipcMain.on('toggleDevTools', function (event) {
  if (event.sender.webContents.WINDOW_ID === 'start') {
    mainWindow.webContents.toggleDevTools()
  }
})

let pdfWindow
const createPdf = function (data) {
  pdfWindow = new BrowserWindow({
    show: false,
    backgroundColor: 'white',
    webPreferences: {
      nodeIntegration: true
    }
  })
  // pdfWindow.webContents.openDevTools()
  // pdfWindow.maximize()
  // pdfWindow.setMenu(null)

  pdfWindow.getData = function () {
    return data
  }

  const pages = []
  let pdfDoc

  async function createPdf () {
    pdfDoc = await PDFDocument.create()
    for (var i = 0; i < pages.length; i++) {
      const doc = await PDFDocument.load(pages[i])
      const [firstDonorPage] = await pdfDoc.copyPages(doc, [0])
      if (i === 0) {
        pdfDoc.addPage(firstDonorPage)
      } else {
        pdfDoc.insertPage(i, firstDonorPage)
      }
    }
    const pdfBytes = await pdfDoc.save()

    dialog.showSaveDialog(mainWindow, { defaultPath: path.join(app.getPath('documents'), 'xxxxxx.pdf') }).then(file => {
      if (file.canceled) {
        mainWindow.webContents.send('pdf-finish', { error: 'canceled' })
      } else {
        fs.writeFileSync(file.filePath, pdfBytes)
        mainWindow.webContents.send('pdf-finish', {})
      }
    }).catch(err => {
      mainWindow.webContents.send('pdf-finish', { error: err })
    })
  }

  pdfWindow.printToPDF = function (pdfOptions, page, pageNumber, next) {
    pdfWindow.webContents.printToPDF(pdfOptions).then(data => {
      // fs.writeFileSync(path.join(__dirname, 'temp', page + '.pdf'), data)
      pages.push(data)
      next()
    }).catch(error => {
      next(error)
    })
  }

  pdfWindow.finish = function () {
    createPdf()
    pdfWindow.close()
    pdfWindow = null
  }

  pdfWindow.loadURL(`file://${__dirname}/pdf.xhtml`)
}

ipcMain.on('createPdf', function (event, data) {
  if (!pdfWindow) {
    createPdf(data)
  }
})

ipcMain.on('openfile', function (event, data) {
  const file = dialog.showOpenDialogSync(mainWindow, {
    defaultPath: app.getPath('documents'),
    properties: ['openFile'],
    filters: [{ name: 'Maestro', extensions: ['mcl'] }]
  })
  const message = { file: file }
  if (file) {
    message.file = file[0]
    message.content = fs.readFileSync(file[0], 'utf8')
  }
  mainWindow.webContents.send('file-load', message)
})
