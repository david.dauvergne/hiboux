module.exports = {
  core: {
    title: 'Core',
    type: 'object',
    description: 'Core description',
    properties: {
      lang: {
        title: 'Langue',
        type: 'string',
        enum: ['en', 'fr'],
        default: 'en'
      }
    }
  }
}
