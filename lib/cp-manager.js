const path = require('path')
const cp = require('./cp')
const { loadComponents, loadTheme } = require('./cp-loader')
const preFormatCss = require('./preFormatCss')
const mEvents = require('./events')

const mIpc = require('../modules/ipc')
const mMonaco = require('../modules/monaco')
const diff = require('../modules/diff')

const {
  removeListener,
  setConfigPluginDefault,
  pref
} = require('./pref-manager')(
  require('../prefs.json'),
  path.join(process.env.APP_USER_DATA, 'user.json'),
  'preferences-update'
)

function assignStyleScheets (ns, data) {
  var id = '__CSS_' + ns
  var idSS = document.getElementById(id)
  if (!idSS) {
    idSS = document.createElement('style')
    idSS.type = 'text/css'
    idSS.id = id
    document.head.appendChild(idSS)
  }
  idSS.innerHTML = data
}

function startApp () {
  window.M.events = mEvents
  cp.start('app')
}

function reloadApp () {
  mIpc.removeAllListeners()
  removeListener()
}

function configDefault (ns, cpsConfig) {
  for (const plugin in cpsConfig) {
    if (Object.entries(cpsConfig[plugin]).length === 0) {
      delete cpsConfig[plugin]
    } else {
      setConfigPluginDefault(ns, plugin, cpsConfig[plugin])
    }
  }
  return cpsConfig
}

function initApp (excludePackages) {
  excludePackages.concat(pref.get('namespace.app.disabledPackages'))
  window.LANG = pref.get('core.lang')
  const cps = loadComponents('app', excludePackages)
  const theme = loadTheme('app', pref.get('namespace.app.theme'))
  // update config default
  cps.config = configDefault('app', cps.config)
  assignStyleScheets('app', theme.css + cps.css)
  window.M = {
    ipc: mIpc,
    monaco: mMonaco,
    diff: diff,
    preferences: pref,
    cp: cp,
    NS: {
      app: {
        themes: theme.themes,
        packages: cps.packages,
        config: cps.config
      }
    }
  }
  cp.prepare('app', cps.cp, cps.plugin)
}

function initComponents (ns) {
  const excludePackages = pref.get(`namespace.${ns}.disabledPackages`)
  const cps = loadComponents(ns, excludePackages)
  const theme = loadTheme(ns, pref.get(`namespace.${ns}.theme`))
  // update config default
  cps.config = configDefault(ns, cps.config)
  const css = preFormatCss(pref.get(`namespace.${ns}.parentNodeName`), theme.css + cps.css)
  assignStyleScheets(ns, css)
  window.M.NS[ns] = {
    themes: theme.themes,
    packages: cps.packages,
    config: cps.config
  }
  M.cp.addComponents(ns, cps.cp)
}

module.exports = {
  startApp,
  initApp,
  initComponents,
  reloadApp
}
