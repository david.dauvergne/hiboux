const fs = require('fs')
const { ipcRenderer } = require('electron')
const { configGetSave, configMergeSave, configMergePlugin } = require('./config-manager')

const cloneOject = function (obj) {
  return JSON.parse(JSON.stringify(obj))
}

const emit = function (eventName, action, oldValue, newValue, stringPath) {
  ipcRenderer.send('allWindow', {
    event: eventName,
    action: action,
    oldValue: oldValue,
    newValue: newValue,
    stringPath: stringPath
  })
}

const dotprop = {
  get: function (obj, str) {
    try {
      var value = str.split('.').reduce(
        function (obj, i) {
          return obj[i]
        }, obj)
      return value
    } catch (err) {}
  },
  has: function (obj, str) {
    try {
      str.split('.').reduce(
        function (obj, i) {
          return obj[i]
        }, obj)
      return true
    } catch (err) {
      return false
    }
  },
  set: function (obj, str, value, eventName) {
    const objPath = str.split('.')
    const oldObj = cloneOject(obj)
    let toEmit = false
    const setValue = function (o, p, v) {
      if (p.length === 1) {
        if (Array.isArray(o[p[0]])) {
          o[p[0]].push(v)
        } else {
          o[p[0]] = v
        }
        toEmit = true
        return
      }
      if (o[p[0]] === undefined) o[p[0]] = {}
      return setValue(o[p[0]], p.slice(1, p.length), v)
    }
    setValue(obj, objPath, value)
    if (toEmit) emit(eventName, 'set', oldObj, obj, str)
    return obj
  },
  del: function (obj, str, value, eventName) {
    const objPath = str.split('.')
    const oldObj = cloneOject(obj)
    let toEmit = false
    const setValue = function (o, p, v) {
      if (p.length === 1) {
        if (o[p[0]] !== undefined) {
          if (Array.isArray(o[p[0]])) {
            const index = o[p[0]].indexOf(v)
            if (index !== -1) {
              o[p[0]].splice(index, 1)
              toEmit = true
            }
          } else {
            delete o[p[0]]
            toEmit = true
          }
        }
        return
      }
      if (o[p[0]] === undefined) return
      return setValue(o[p[0]], p.slice(1, p.length), v)
    }
    setValue(obj, objPath, value)
    if (toEmit) emit(eventName, 'del', oldObj, obj, str)
    return obj
  }
}

module.exports = function (config, prefPath, eventName) {
  let OPTIONS = {}
  const ref = {
    file: prefPath,
    ext: '.json',
    content: {}
  }
  if (fs.existsSync(prefPath)) {
    try {
      ref.content = fs.readFileSync(prefPath, 'utf8')
      OPTIONS = configMergeSave(cloneOject(config), JSON.parse(ref.content))
    } catch (err) {
      console.linter.parse(err, ref)
      OPTIONS = cloneOject(config)
    }
  } else {
    OPTIONS = cloneOject(config)
  }

  const ipcListener = (event, message) => {
    if (message.WINDOW_ID === window.WINDOW_ID) {
      fs.writeFileSync(prefPath, JSON.stringify(configGetSave(OPTIONS, config)))
    } else {
      OPTIONS = message.newValue
    }
  }

  ipcRenderer.on(eventName, ipcListener)

  return {
    removeListener: function removeListener () {
      ipcRenderer.removeListener(eventName, ipcListener)
    },
    setConfigPluginDefault: function setConfigPluginDefault (ns, pluginName, pluginConfig) {
      config = configMergePlugin(ns, cloneOject(config), pluginName, pluginConfig)
      OPTIONS = configMergeSave(cloneOject(config), OPTIONS)
    },
    pref: {
      get: function (keyPath) {
        return dotprop.get(OPTIONS, keyPath)
      },
      set: function (keyPath, value) {
        dotprop.set(OPTIONS, keyPath, value, eventName)
      },
      has: function (keyPath) {
        return dotprop.has(OPTIONS, keyPath)
      },
      delete: function (keyPath, value) {
        dotprop.del(OPTIONS, keyPath, value, eventName)
      }
    }
  }
}
