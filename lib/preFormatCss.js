
const postcss = require('postcss')
const parser = require('postcss-selector-parser')

const createParser = function (parentElName) {
  return parser(function (root) {
    root.walkTags(function (tag) {
      if (tag.parent.nodes[0] === tag) {
        tag.value = parentElName + ' ' + tag
      }
    })
  })
}

const plugin = postcss.plugin('assignNamespace', function (parentElName) {
  var selectorProcessor = createParser(parentElName)
  return function (root) {
    root.walkComments(function (comment) {
      comment.remove()
    })
    root.walkRules(function (rule) {
      var x = selectorProcessor.processSync(rule)
      rule.selector = x
    })
  }
})

const SPACES_LINE = /[\r\n\t]+/g
const SPACES_GAPS = /([,:;{}])\s+/g

module.exports = function (parentElName, source) {
  return postcss([plugin(parentElName)]).process(source.replace(SPACES_LINE, '').replace(SPACES_GAPS, '$1')).css
}
