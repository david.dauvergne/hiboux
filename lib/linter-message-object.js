const msg = {
  severity: '',
  excerpt: '',
  situation: '',
  location: {
    file: '',
    position: {
      start: { line: 0, column: 0 },
      end: { line: 0, column: 0 }
    }
  }
}

module.exports = function (lintSimply = {}) {
  const newMsg = JSON.parse(JSON.stringify(msg))
  if (lintSimply.severity) {
    newMsg.severity = lintSimply.severity
  }
  if (lintSimply.excerpt) {
    newMsg.excerpt = lintSimply.excerpt
  }
  if (lintSimply.file) {
    newMsg.location.file = lintSimply.file
  }
  if (lintSimply.situation) {
    newMsg.location.situation = lintSimply.situation
  }
  if (lintSimply.lineStart) {
    newMsg.location.position.start.line = lintSimply.lineStart
  }
  if (lintSimply.coulumnStart) {
    newMsg.location.position.start.column = lintSimply.coulumnStart
  }
  if (lintSimply.lineEnd) {
    newMsg.location.position.end.line = lintSimply.lineEnd
  }
  if (lintSimply.coulumnEnd) {
    newMsg.location.position.end.column = lintSimply.coulumnEnd
  }
  return newMsg
}
