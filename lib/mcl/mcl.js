const MCL = {
  namespace: 'http://www.mcl.org/1.0',
  prefix: 'mcl',
  prefixSeparator: ':',
  defineTagName: 'define',
  slotTagName: 'slot',
  slotShortTagName: '✀',
  importationTagName: 'importation',
  importationShortTagName: '✈',
  caret: '❚',
  chars: {
    '\t': 'TAB',
    '\n': 'NEWLINE',
    '\r': 'NEWLINE',

    '❗': 'COMMENT',

    '❬': 'TAG_OPEN',
    '❭': 'TAG_CLOSE',

    '❲': 'ATTR_OPEN',
    '❳': 'ATTR_CLOSE',
    '➔': 'ATTR_ASSIGN',
    '❘': 'ATTR_SEPARATOR',

    '❛': 'INCLUDES_OPEN',
    '❜': 'INCLUDES_CLOSE',

    '❨': 'CDATA_OPEN',
    '❩': 'CDATA_CLOSE',

    '❚': 'CARET'
  },
  regex: {
    tagBlock: /^((?:([a-zA-Z]+)(:))?([-_✈✀a-zA-Z0-9]+))$/,
    tagInline: /^((?:([a-zA-Z]+)(:))?([-_✀a-zA-Z0-9]+))$/,
    attribute: /^((?:([a-zA-Z]+)(:))?([-_a-zA-Z0-9]+))$/,
    name: /^([_a-zA-Z][-_a-zA-Z0-9]*)$/
  },
  elements: {}
}

// long name
MCL.defineLongTagName = MCL.prefix + MCL.prefixSeparator + MCL.defineTagName
MCL.slotLongTagName = MCL.prefix + MCL.prefixSeparator + MCL.slotTagName
MCL.importationLongTagName = MCL.prefix + MCL.prefixSeparator + MCL.importationTagName

// elements definition
MCL.elements[MCL.importationTagName] = { features: [{ name: 'file' }] }
MCL.elements[MCL.importationShortTagName] = { features: [{ name: 'file' }] }
MCL.elements[MCL.defineTagName] = { features: [{ name: 'name' }] }

module.exports = MCL
