const MCL = require('./mcl')
const MCLREGEX = MCL.regex
const MCLCHARS = MCL.chars

module.exports = function (emitter) {
  let stop, state, line, column, tabsBefore, tabs, data, attrPos, attrName, index, file, tagInlines

  const set = function set (name, detail) {
    emitter(name, {
      line: line,
      column: column,
      tabsBefore: tabsBefore,
      tabs: tabs,
      attrPos: attrPos,
      index: index,
      data: data,
      detail: detail,
      file: file
    })
  }
  const createError = function createError (start, end, excerpt) {
    stop = true
    const err = {
      severity: 'error',
      file: file,
      lineStart: line + 1,
      coulumnStart: start,
      lineEnd: line + 1,
      coulumnEnd: end,
      excerpt: excerpt
    }
    console.linter.send(err)
    return err
  }

  const setError = function setError (excerpt) {
    const err = createError(column - data.length, column, excerpt)
    emitter('error', err)
  }

  const setErrorIndent = function setErrorIndent (excerpt) {
    const err = createError(0, tabs, excerpt)
    emitter('error', err)
  }

  const resetLine = function resetLine () {
    state = 'start'
    line++
    column = -1
    tabsBefore = tabs
    tabs = 0
  }

  const addChar = function addChar (char) {
    data += char
  }

  const addText = function addText (inCaret) {
    if (data !== '') {
      if (inCaret) {
        const c = data.split('❚')
        if (c.length > 1) {
          data = c[0]
          const _column = column
          column = column - c[1].length
          set('text')
          set('caret_tag')
          data = c[1]
          column = _column
          set('text')
        } else {
          set('text')
        }
      } else {
        set('text')
      }
      data = ''
    }
  }

  const addTextChangeState = function addTextChangeState (newstate, inCaret) {
    addText(inCaret)
    state = newstate
  }

  const tagInlineOpen = function tagInlineOpen () {
    tagInlines++
    addTextChangeState('tag_inline', true)
  }

  const tagInlineClose = function tagInlineClose () {
    tagInlines--
    set('close_tag_inline')
  }

  const caretTestAttribute = function caretTestAttribute () {
    if (data.indexOf(MCL.caret) > -1) {
      set('caret_attribute', 'self')
      data = data.replace(MCL.caret, '')
    }
  }

  const validAttrName = function validAttrName () {
    caretTestAttribute()
    const m = data.match(MCLREGEX.attribute)
    if (m && MCLREGEX.name.test(m[4])) {
      return { qName: m[1], prefix: m[2], localPart: m[4] }
    }
    setError('Invalid attribute name')
    return false
  }

  const addAttrName = function addAttrName () {
    attrName = validAttrName()
    if (attrName) {
      data = ''
    }
  }

  const addAttribute = function addAttribute () {
    if (attrName) {
      caretTestAttribute()
      set('attribute', attrName)
      attrName = ''
    } else {
      setError('Invalid attribute')
    }
  }
  const validCloseAttribute = function validCloseAttribute () {
    if (attrName) {
      setError('No attributes close')
      return false
    }
    return true
  }

  const attrClose = function attrClose () {
    addAttribute()
    set('attribute_close')
    changeState('text')
    attrPos = 0
  }

  const attrSeparator = function attrSeparator () {
    addAttribute()
    data = ''
    attrPos++
  }

  const validIndent = function validIndent () {
    if (!(tabs <= tabsBefore + 1)) {
      setErrorIndent('Invalid indentation')
      return false
    }
    return true
  }

  const validTagInlinesClosed = function validTagInlinesClosed () {
    if (tagInlines !== 0) {
      const err = createError(-1, column, 'Error close inline tag')
      emitter('error', err)
      return false
    }
    return true
  }

  const validBlockTagName = function validBlockTagName () {
    const m = data.match(MCLREGEX.tagBlock)
    if (m) {
      if (m[4] === MCL.importationShortTagName) {
        return {
          qName: MCL.importationLongTagName,
          prefix: MCL.prefix,
          localPart: MCL.importationTagName
        }
      } else if (MCLREGEX.name.test(m[4])) {
        if (!m[2]) {
          m[2] = '_' // default prefix
        }
        return { qName: m[1], prefix: m[2], localPart: m[4] }
      }
    }
    setError('Invalid tag block name')
    return false
  }

  const validInlineTagName = function validInlineTagName () {
    const m = data.match(MCLREGEX.tagInline)
    if (m) {
      if (MCLREGEX.name.test(m[4])) {
        if (!m[2]) {
          m[2] = '_' // default prefix
        }
        return { qName: m[1], prefix: m[2], localPart: m[4] }
      }
    }
    setError('Invalid tag inline name')
    return false
  }

  const validTextBeforeCdata = function validTextBeforeCdata () {
    if (data !== '') {
      setError('Invalid text before cdata')
      return
    }
    state = 'text_cdata'
  }

  const changeState = function changeState (newstate) {
    data = ''
    state = newstate
  }

  const next = function next (char) {
    const action = states[state][MCLCHARS[char]] || states[state].CHAR
    action(char)
  }

  const states = {
    start: {
      TAB: function () {
        tabs++
      },
      NEWLINE: function () {
        set('empty_line')
        line++
        column = -1
        tabs = 0
      },
      COMMENT: function () {
        if (validIndent()) {
          state = 'comment_block'
        }
      },
      CHAR: function (char) {
        data += char
        state = 'tag_block'
      },
      CARET: function () {
        set('caret_attribute', line)
      }
    },
    comment_block: {
      NEWLINE: function () {
        set('comment_block')
        data = ''
        state = 'start'
        line++
        column = -1
        tabs = 0
      },
      CHAR: addChar,
      CARET: function () {
        set('caret_attribute', 'self')
      }
    },
    tag_block: {
      NEWLINE: function () {
        if (validIndent() && validTagInlinesClosed()) {
          const tagName = validBlockTagName()
          if (tagName) {
            set('open_tag_block', tagName)
            data = ''
            set('close_tag_block')
            resetLine()
          }
        }
      },
      ATTR_OPEN: function () {
        if (validIndent()) {
          const tagName = validBlockTagName()
          if (tagName) {
            set('open_tag_block', tagName)
            changeState('tag_block_open_attr')
          }
        }
      },
      CHAR: addChar,
      CARET: function () {
        set('caret_in_tag', 'self')
      }
    },
    tag_block_open_attr: {
      NEWLINE: function () {
        if (validCloseAttribute()) {
          addText(true)
          set('close_tag_block')
          resetLine()
        }
      },
      CDATA_OPEN: validTextBeforeCdata,
      CHAR: addChar,
      ATTR_CLOSE: attrClose,
      ATTR_ASSIGN: addAttrName,
      ATTR_SEPARATOR: attrSeparator,
      TAG_OPEN: tagInlineOpen
    },
    text: {
      NEWLINE: function () {
        if (validTagInlinesClosed()) {
          addText()
          set('close_tag_block')
          resetLine()
        }
      },
      CDATA_OPEN: validTextBeforeCdata,
      TAG_OPEN: tagInlineOpen,
      TAG_CLOSE: function () {
        addText()
        tagInlineClose()
      },
      CHAR: addChar,
      CARET: function () {
        addText()
        set('caret_tag')
      }
    },
    tag_inline: {
      CHAR: addChar,
      NEWLINE: function () {
        if (validTagInlinesClosed()) {
          // addChar(char)
        }
      },
      TAG_CLOSE: function () {
        const tagName = validInlineTagName()
        if (tagName) {
          set('open_tag_inline', tagName)
          changeState('text')
        }
        tagInlineClose()
      },
      ATTR_OPEN: function () {
        const tagName = validInlineTagName()
        if (tagName) {
          set('open_tag_inline', tagName)
          changeState('tag_inline_open_attr')
        }
      },
      CARET: function () {
        set('caret_in_tag', 'self')
      }
    },
    tag_inline_open_attr: {
      TAG_CLOSE: function () {
        if (validCloseAttribute()) {
          addTextChangeState('text', true)
          tagInlineClose()
        }
      },
      CDATA_OPEN: validTextBeforeCdata,
      CHAR: addChar,
      ATTR_CLOSE: attrClose,
      ATTR_ASSIGN: addAttrName,
      ATTR_SEPARATOR: attrSeparator,
      TAG_OPEN: tagInlineOpen
    },
    text_cdata: {
      CHAR: addChar,
      NEWLINE: function (char) {
        data += char
        line++
        column = -1
      },
      CDATA_CLOSE: function () {
        set('cdata')
        changeState('text')
      },
      CARET: function () {
      // nothing
      }
    },
    includes: {
      CHAR: addChar,
      INCLUDES_CLOSE: function () {
        if (MCLREGEX.name.test(data)) {
          set('includes')
        } else {
          setError('Invalid include name')
        }
        changeState('text')
      }
    }
  }

  return function (source, content) {
    emitter('init')
    file = source
    stop = false
    state = 'start'
    line = 0
    column = -1
    tabsBefore = 0
    tabs = 0
    data = ''
    attrPos = 0
    attrName = ''
    tagInlines = 0
    content = content + '\n'
    const len = content.length
    for (index = 0; index < len; index++) {
      if (stop) break
      column++
      next(content[index])
    }
    return emitter('done', { stop: stop, line: line })
  }
}
