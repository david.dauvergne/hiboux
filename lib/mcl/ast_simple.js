const fs = require('fs')
const path = require('path')
const MCL = require('./mcl')

const toAST = function (lexer, PN, startIndent = 0) {
  let rootNode, current, error, attributes, emptylines, oldline, caretInTag

  function getCurrent (d, current) {
    if ((d.tabs === d.tabsBefore) && !current.isRoot) {
      current = current[PN.parentNode]
    } else if (d.tabs < d.tabsBefore) {
      const length = (d.tabsBefore - d.tabs) + 1
      for (let i = 0; i < length; i++) {
        current = current[PN.parentNode]
      }
    }
    return current
  }

  function createRootNode () {
    const rootNode = {
      isRoot: true,
      [PN.type]: 'tag',
      name: '____root___',
      importation: {},
      [PN.attributes]: {},
      [PN.parentNode]: null,
      [PN.childNodes]: [],
      [PN.coulumnStart]: []
    }
    return rootNode
  }

  function createNodeText (params) {
    return Object.assign({
      [PN.parentNode]: null
    }, params)
  }

  function createNode (params) {
    return Object.assign({
      [PN.type]: 'tag',
      [PN.isBlock]: true,
      [PN.parentNode]: null,
      [PN.attributes]: {},
      [PN.childNodes]: [],
      [PN.coulumnStart]: []
    }, params)
  }

  const pushNode = function pushNode (node) {
    current[PN.childNodes].push(node)
    node[PN.parentNode] = current
    return node
  }

  const setError = function setError (excerpt, d) {
    const err = {
      severity: 'error',
      file: d.file,
      lineStart: d.line + 1,
      coulumnStart: d.column - d.data.length,
      lineEnd: d.line + 1,
      coulumnEnd: d.column,
      excerpt: excerpt
    }
    console.linter.send(err)
    error.push(err)
  }

  const testCaretInNode = function testCaretInNode () {
    if (current.caret) {
      current[PN.attributes].__caret = current.caret
      delete current.caret
    } else if (caretInTag) {
      current[PN.attributes].__caret = caretInTag
      caretInTag = null
    }
  }

  const testImport = function testImport (d) {
    if (current[PN.tagName] === MCL.importationLongTagName) {
      if (current[PN.attributes].file) {
        const file = path.resolve(path.dirname(d.file), current[PN.attributes].file + '.mcl')
        if (fs.existsSync(file)) {
          const parser = lexer(toAST(lexer, PN, current[PN.indent]))
          parser(file, fs.readFileSync(file, 'utf8'), function (e, data) {
            if (e) {
              error = error.concat(e)
            }
            if (data) {
              data[PN.childNodes].forEach((child) => {
                current[PN.parentNode][PN.childNodes].push(child)
              })
              current[PN.parentNode][PN.childNodes]
                .splice(current[PN.parentNode][PN.childNodes].indexOf(current), 1)
            }
          })
        } else {
          // error no file exist
          setError(`The file "${current[PN.attributes].file}" does not exist`, d)
        }
      } else {
        // error no property file
        setError('No property file', d)
      }
    }
  }

  const actions = {
    error: function (err) {
      error.push(err)
    },
    init: function () {
      rootNode = createRootNode()
      current = rootNode
      caretInTag = null
      error = []
      attributes = {}
      emptylines = []
      oldline = -1
    },
    done: function (d) {
      this.empty_line(d)
      const result = { error: false, root: null, lines: emptylines }
      if (d.stop) {
        result.error = error
      } else {
        result.root = rootNode
        if (error) {
          result.error = error
        }
      }
      return result
    },
    caret_in_tag: function (d) {
      caretInTag = d.detail
    },
    caret_attribute: function (d) {
      current.caret = d.detail
    },
    caret_tag: function () {
      current[PN.childNodes].push(createNode({
        [PN.tagName]: '__caret__',
        [PN.childNodes]: [{ [PN.type]: 'text', data: '-' }]// no break space
      }))
      current[PN.coulumnStart].push(null)
    },
    text: function (d) {
      pushNode(createNodeText({
        [PN.type]: 'text',
        data: d.data
      }))
      current[PN.coulumnStart].push(d.column - d.data.length)
    },
    attribute: function (d) {
      attributes[d.detail.qName] = d.data
    },
    attribute_close: function (d) {
      current[PN.attributes] = attributes
      attributes = {}
    },
    empty_line: function (d) {
      if (oldline !== -1 && oldline !== d.line - 1) {
        emptylines.push(oldline)
      }
      oldline = d.line
    },
    comment_block: function (d) {
      // const oldCurrent = current
      // current = getCurrent(d, current)
      // // pushNode(createNodeText({
      // //   [PN.type]: 'comment',
      // //   data: d.data,
      // //   [PN.indent]: startIndent + d.tabs,
      // //   [PN.line]: d.line
      // // }))
      // current = oldCurrent
    },
    open_tag_block: function (d) {
      current = getCurrent(d, current)
      current[PN.coulumnStart].push(null)
      current = pushNode(createNode({
        [PN.tagName]: d.detail.qName,
        [PN.indent]: startIndent + d.tabs
      }))
    },
    close_tag_block: function (d) {
      current[PN.attributes].__line = String(d.line)
      testCaretInNode()
      testImport(d)
    },
    open_tag_inline: function (d) {
      current[PN.coulumnStart].push(null)
      current = pushNode(createNode({
        [PN.tagName]: d.detail.qName,
        [PN.isBlock]: false
      }))
    },
    close_tag_inline: function (d) {
      current[PN.attributes].__line = String(d.line)
      testCaretInNode()
      current = current[PN.parentNode]
    },
    cdata: function (d) {
      pushNode(createNodeText({ [PN.type]: 'cdata', data: d.data }))
    }
  }
  return function (type, name) {
    return actions[type](name)
  }
}

module.exports = toAST
