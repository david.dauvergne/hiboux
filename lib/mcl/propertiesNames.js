module.exports = function (pn) {
  const PN = {
    type: 'type',
    tagName: 'name',
    attributes: 'attributes',
    properties: 'properties',
    nextSibling: 'next',
    previousSibling: 'prev',
    parentNode: 'parent',
    childNodes: 'children',
    indent: 'indent',
    isBlock: 'isBlock',
    line: 'line',
    coulumnStart: 'coulumnStart'
  }
  if (pn) {
    Object.assign(PN, pn)
  }
  return PN
}
