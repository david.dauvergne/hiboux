const lexer = require('./mcl/lexer_simple')
const astSimple = require('./mcl/ast_simple')
const PNCpageFnc = require('./mcl/propertiesNames')

// assign properties names
const PNComponent = PNCpageFnc({
  parentNode: '__parent__',
  indent: '__indent__',
  isBlock: '__isBlock__'
})

const _parserComponent = lexer(astSimple(lexer, PNComponent))

module.exports = function (file, content) {
  var data = _parserComponent(file, content)
  const tpls = {}
  if (data.error.length === 0 && data.root) {
    for (var node of data.root[PNComponent.childNodes]) {
      if (node[PNComponent.type] === 'tag' && node[PNComponent.tagName] === 'template') {
        if (node[PNComponent.attributes] && node[PNComponent.attributes].id && node[PNComponent.childNodes]) {
          const id = node[PNComponent.attributes].id
          delete node[PNComponent.attributes].id
          tpls[id] = node[PNComponent.attributes]
          tpls[id].content = node[PNComponent.childNodes]
        }
      }
    }
  }
  return tpls
}
