const path = require('path')
const amdLoader = require('monaco-editor/dev/vs/loader.js')
const amdRequire = amdLoader.require

var uriFromPath = function (_path) {
  var pathName = path.resolve(_path).replace(/\\/g, '/')
  if (pathName.length > 0 && pathName.charAt(0) !== '/') {
    pathName = '/' + pathName
  }
  return encodeURI('file://' + pathName)
}
amdRequire.config({
  baseUrl: uriFromPath(path.join(__dirname, '../node_modules/monaco-editor/min'))
})

module.exports = new Promise(function (resolve) {
  amdRequire(['vs/editor/editor.main'], function () {
    monaco.languages.register({ id: 'mcl' })
    monaco.languages.setMonarchTokensProvider('mcl', {
      tokenizer: {
        root: [
          // comment
          [/^\s*❗.*$/, 'comment'],
          // tag
          [/^\t*((?:[a-zA-Z0-9]+:)?[-_✈a-zA-Z0-9]+)/, 'tag-name'],
          [/❲/, 'attr-bracket'],
          [/([^❲❘]+)(➔)([^❳❘]+)(❘)/, ['attr-key', 'attr-equal', 'attr-value', 'attr-separator']],

          [/([^❲❘]+)(➔)([^❳❘]+)(❳)/, ['attr-key', 'attr-equal', 'attr-value', 'attr-bracket']],

          [/❨/, { token: 'cdata-bracket', next: '@cdata' }],

          [/(❬)((?:[a-zA-Z0-9]+:)?[-_✈a-zA-Z0-9]+)/, ['tag-bracket', 'tag-name']],
          [/❭/, 'tag-bracket'],
          [/ /, 'nobreakspace']
        ],
        cdata: [
          [/[^❩]+/, 'cdata-text'],
          [/❩/, { token: 'cdata-bracket', next: '@pop' }]
        ]
      }
    })
    resolve(monaco)
  })
})
