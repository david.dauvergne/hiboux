const util = require('util')
const ipc = require('electron').ipcRenderer
const lmo = require('./linter-message-object')

function initLintError (severity, ref) {
  return lmo({ severity: severity, file: ref.file })
}

function getErrorLine (err, index, lintSimply) {
  const stack = err.stack.split('\n')
  const errLine = stack[index].match(/:(\d+):(\d+)\)/)
  if (errLine) {
    lintSimply.lineStart = errLine[1]
    lintSimply.coulumnStart = errLine[2]
  }
  return lintSimply
}

function _send (err) {
  if (ipc.eventNames().includes('error')) {
    ipc.send('allWindow', { event: 'error', type: 'error', err: [err] })
  } else {
    let situation = ''
    if (err.location.situation && err.location.situation !== '') {
      situation = ` (${err.location.situation})`
    }
    const _err = `%c${err.excerpt}${situation}
  %cFile: %c${err.location.file}
  %cLine:      %c${err.location.position.start.line}
  %cColumn:    %c${err.location.position.start.column}`
    console[err.severity](_err, 'font-weight: bold;', 'font-weight: normal; color:tan;', 'color:inherit;', 'color:tan;', 'color:inherit;', 'color:tan;', 'color:inherit;')
  }
}

console.devTools = {
  toogle: function toogle () {
    ipc.send('toggleDevTools')
  }
}

console.test = {
  assert: function assert (message, test, result) {
    result = result.toString()
    ipc.send('allWindow', {
      event: 'error',
      type: 'assert',
      message: {
        message: message,
        result: result,
        test: (test === result)
      }
    })
  },
  title: function title (message) {
    ipc.send('allWindow', { event: 'error', type: 'title', message: message })
  },
  comment: function comment (message) {
    ipc.send('allWindow', { event: 'error', type: 'comment', message: message })
  }
}

console.linter = {
  clear: function clear () {
    ipc.send('allWindow', { event: 'error', type: 'nothing', clear: true })
  },
  parse: function parse (err, ref) {
    console.log(err)
    const lintErr = initLintError('error', ref)
    let syntaxErr, lines
    let line = 0
    if (ref.ext === '.json') {
      syntaxErr = err.message.match(/position\s+(\d+)/i)
      if (syntaxErr) {
        lines = ref.content.split('\n')
        const position = syntaxErr[1] * 1
        let countPosition = 0

        for (let i = 0; i < lines.length; i++) {
          const p = countPosition + lines[i].length + 1
          line++
          if (position <= p) {
            lintErr.location.position.start.column = position - countPosition + 1
            lintErr.location.position.end.column = lintErr.location.position.start.column
            break
          }
          countPosition = p
        }
        lintErr.excerpt = err.name + ': ' + err.message.split('\n')[0]
      } else {
        syntaxErr = err.message.match(/^Unexpected end of JSON.*/i)
        if (syntaxErr) {
          lines = ref.content.trim().split('\n')
          line = lines.length
          lintErr.location.position.start.column = lines[lines.length - 1].length + 1
          lintErr.location.position.end.column = lintErr.location.position.start.column
        }
        lintErr.excerpt = err.name + ': ' + err.message
      }
    } else {
      const rawLines = util.inspect(err).split('\n')
      const fileLine = rawLines[0].split(':')
      line = fileLine.pop()
      if (isNaN(line)) {
        line = 0
      }

      const posColumnStart = rawLines[2].indexOf('^')
      if (posColumnStart > -1) {
        lintErr.location.position.start.column = posColumnStart + 1
        lintErr.location.position.end.column = lintErr.location.position.start.column
        const posColumnStop = rawLines[2].lastIndexOf('^')
        if (posColumnStop > -1) {
          lintErr.location.position.end.column = posColumnStop + 2
        }
      }
      lintErr.excerpt = err.name + ': ' + err.message
    }
    lintErr.location.position.start.line = line
    lintErr.location.position.end.line = line
    _send(lintErr)
  },
  send: function send (data) {
    _send(lmo(data))
  },
  generateError: function generateError (typeError, message, action, actionName, element) {
    try {
      switch (typeError) {
        case 'ReferenceError':
          throw new ReferenceError(message)
        case 'SyntaxError':
          throw new SyntaxError(message)
        case 'TypeError':
          throw new TypeError(message)
        case 'RangeError':
          throw new RangeError(message)
        default:
          throw Error(message)
      }
    } catch (err) {
      let lintSimply = {
        severity: 'error',
        file: `${element.__$package}/${element.__$name}.js`,
        excerpt: err.name + ': ' + err.message,
        situation: `${action}: ${actionName}`
      }
      lintSimply = getErrorLine(err, 3, lintSimply)
      _send(lmo(lintSimply))
    }
  },
  postError: function postError (err, element, action, actionName) {
    console.log(err)
    let lintSimply = {
      severity: 'error',
      file: `${element.__$package}/${element.__$name}.js`,
      excerpt: err.name + ': ' + err.message,
      situation: `${action}: ${actionName}`
    }
    const stack = err.stack.split('\n')
    const modules = stack[1].match(/\/modules\//)
    if (modules) {
      const moduleName = stack[1].match(/\/([^/]*).js:(\d+):(\d+)\)/i)
      lintSimply.situation += `, Module: ${moduleName[1]}`
    }
    lintSimply = getErrorLine(err, 1, lintSimply)
    _send(lmo(lintSimply))
  }
}
