const getTag = function (value) {
  return Object.prototype.toString.call(value)
}

function isObject (value) {
  return typeof value === 'object' && value !== null && typeof value !== 'function' && !Array.isArray(value)
}

function isArray (value) {
  return Array.isArray(value)
}

function isBoolean (value) {
  return value === true || value === false || getTag(value) === '[object Boolean]'
}

function arraysMatch (arr1, arr2) {
  if (arr1.length !== arr2.length) return false
  for (var i = 0; i < arr1.length; i++) {
    if (arr1[i] !== arr2[i]) return false
  }
  return true
}

function configGetSave (base, other) {
  const diff = {}
  for (const prop in base) {
    if (isObject(base[prop]) && isObject(other[prop])) {
      const result = configGetSave(base[prop], other[prop])
      if (Object.getOwnPropertyNames(result).length) {
        diff[prop] = result
      }
    } else if (isArray(base[prop]) && isArray(other[prop])) {
      if (!arraysMatch(base[prop], other[prop])) {
        diff[prop] = base[prop]
      }
    } else if (isBoolean(base[prop]) && base[prop] !== other[prop]) {
      diff[prop] = !diff[prop]
    } else if (!other[prop]) {
      diff[prop] = base[prop]
    } else if (base[prop] !== other[prop]) {
      diff[prop] = base[prop]
    }
  }

  return diff
}

function configMergeSave (base, other) {
  const obj = {}
  for (const prop in base) {
    if (isObject(base[prop]) && isObject(other[prop])) {
      if (!Object.keys(base[prop]).length) {
        obj[prop] = other[prop]
      } else {
        const result = configMergeSave(base[prop], other[prop])
        if (Object.getOwnPropertyNames(result).length) {
          obj[prop] = result
        }
      }
    } else if (isArray(base[prop]) && isArray(other[prop])) {
      if (!arraysMatch(base[prop], other[prop])) {
        obj[prop] = other[prop]
      } else {
        obj[prop] = base[prop]
      }
    } else if (isBoolean(base[prop]) && base[prop] !== other[prop]) {
      obj[prop] = other[prop]
    } else if (!other[prop]) {
      obj[prop] = base[prop]
    } else if (base[prop] !== other[prop]) {
      obj[prop] = other[prop]
    } else {
      obj[prop] = base[prop]
    }
  }

  return obj
}

function getDefault (base) {
  const obj = {}
  for (const prop in base) {
    if (isObject(base[prop]) && base[prop].type === 'object') {
      obj[prop] = getDefault(base[prop].properties)
    } else if (isObject(base[prop])) {
      obj[prop] = getDefault(base[prop])
    } else if (prop === 'default') {
      return base[prop]
    }
  }
  return obj
}

function configMergePlugin (ns, base, name, jsonSchema) {
  base.namespace[ns].packages[name] = getDefault(jsonSchema)
  return base
}

module.exports = {
  configGetSave, // change, default
  configMergeSave, // default, save
  configMergePlugin // default, name, jsonSchema
}
