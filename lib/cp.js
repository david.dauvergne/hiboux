const _validPosition = ['beforebegin', 'afterbegin', 'beforeend', 'afterend', 'replace', 'empty']

let CPS, ids, METHODS, PROPERTIES, ALLCP

function removeProperties (ns, props, elID) {
  if (PROPERTIES[ns] && props) {
    props.forEach(function (prop) {
      var index = PROPERTIES[ns][prop].indexOf(elID)
      if (index > -1) PROPERTIES[ns][prop].splice(index, 1)
    })
  }
}

function removeMethods (ns, methods, elID) {
  if (METHODS[ns] && methods) {
    methods.forEach(function (method) {
      var index = METHODS[ns][method].indexOf(elID)
      if (index > -1) METHODS[ns][method].splice(index, 1)
    })
  }
}

function getProperties (ns, prop) {
  if (PROPERTIES[ns] && PROPERTIES[ns][prop]) {
    return PROPERTIES[ns][prop].map(function (id) {
      if (ALLCP[id]) {
        return ALLCP[id][prop]
      }
    })
  } else {
    return null
  }
}

function dispatchMethods (ns, prop) {
  if (METHODS[ns] && METHODS[ns][prop]) {
    var values = getProperties(ns, prop)
    if (values) {
      METHODS[ns][prop].forEach(function (id) {
        if (ALLCP[id]) {
          ALLCP[id][prop](values)
        }
      })
    }
  }
}

function addALLCP (element) {
  if (element.__$id) {
    ALLCP[element.__$id] = element
    element.__$properties.forEach(function (prop) {
      dispatchMethods(element.__$ns, prop)
    })
    element.__$methods.forEach(function (method) {
      dispatchMethods(element.__$ns, method)
    })
  }
}

function rmALLCP (element) {
  if (element.nodeType === 1 && ALLCP[element.__$id]) {
    delete ALLCP[element.__$id]
    const id = element.__$id
    const properties = element.__$properties
    const ns = element.__$ns
    removeProperties(ns, properties, id)
    removeMethods(ns, element.__$methods, id)
    properties.forEach(function (prop) {
      dispatchMethods(ns, prop)
    })
  }
}

const observer = new MutationObserver(function (mutations) {
  mutations.forEach(function (mutation) {
    if (mutation.removedNodes.length) {
      for (let i = 0; i < mutation.removedNodes.length; i++) {
        const elr = mutation.removedNodes[i]
        if (elr.nodeType === 1 && ALLCP[elr.__$id]) {
          [].forEach.call(elr.querySelectorAll('*'), function (elrc) {
            rmALLCP(elrc)
          })
          rmALLCP(elr)
        }
      }
    }

    if (mutation.addedNodes.length) {
      for (let j = 0; j < mutation.addedNodes.length; j++) {
        const ela = mutation.addedNodes[j]
        if (ela.__$id) {
          [].forEach.call(ela.querySelectorAll('*'), function (elac) {
            addALLCP(elac)
          })
          addALLCP(ela)
        }
      }
    }
  })
})

observer.observe(document.documentElement, { childList: true, subtree: true, characterData: false })

function __assignProperties (me, prop, el) {
  if (!me.__$propertiesEls[prop]) me.__$propertiesEls[prop] = []
  me.__$propertiesEls[prop].push(el)
}

function __dispatchProperties (me, prop, value) {
  if (me.__$propertiesEls[prop]) {
    me.__$propertiesEls[prop].forEach(function (el) {
      el.innerHTML = value
    })
  }
}

function __assignAttributes (me, attr, el) {
  if (!me.__$attributesEls[attr]) me.__$attributesEls[attr] = []
  me.__$attributesEls[attr].push(el)
}

function __dispatchAttributes (me, attr, value) {
  if (me.__$attributesEls[attr]) {
    me.__$attributesEls[attr].forEach(function (el) {
      el.innerHTML = value
    })
  }
}

function _getOverlay (me, overlay, overlayElement) {
  if (overlayElement) return overlayElement
  if (overlay === ':scope') return me
  let overlayEl = me.els[overlay]
  if (!overlayEl) overlayEl = me.querySelector(overlay)
  if (!overlayEl) overlayEl = document.querySelector(overlay)
  return overlayEl
}

function setProperty (el, prop) {
  try {
    Object.defineProperty(el, prop, {
      set: function (val) {
        this['__' + prop] = val
        __dispatchProperties(el, prop, val)
        dispatchMethods(el.__$ns, prop)
      },
      get: function () {
        return this['__' + prop]
      }
    })
  } catch (e) {}
}

function _assignMethod (element, actionName, actionType, mode) {
  return function () {
    try {
      return CPS[element.__$ns][element.__$name][mode][actionName].apply(element, arguments)
    } catch (err) {
      console.linter.postError(err, element, actionName, actionType)
    }
  }
}

function _assignEvent (element, actionName, cEvent, actionType) {
  return function (e) {
    try {
      cEvent.apply(element, [e])
    } catch (err) {
      console.linter.postError(err, element, actionName, actionType)
    }
  }
}

function _preparEvents (el, els, events) {
  for (const ev in events) {
    if (typeof events[ev] === 'function') {
      el.addEventListener(ev, _assignEvent(el, 'Event', events[ev], ev))
    } else {
      // delegation
      if (els[ev]) {
        for (const delegationEv in events[ev]) {
          els[ev].addEventListener(delegationEv, _assignEvent(
            el,
            `Event delegation "${ev}"`,
            events[ev][delegationEv],
            delegationEv
          ))
        }
      } else {
        console.linter.generateError('ReferenceError', `Element "${ev}" does not exist in component: "${el.nodeName}"`, 'Event', 'delegation', el)
      }
    }
  }
}

function prepareElement (ns, el, name) {
  const C = CPS[ns][name]
  ids++
  el.__$id = 'nm-' + ids
  el.__$methods = []
  el.__$properties = []
  el.__$propertiesEls = {}
  el.__$attributesEls = {}
  el.els = {}
  el.__$package = C.package

  // =======================================================
  // attributes
  const AttributesHandler = {
    set: function (obj, prop, value) {
      obj[prop] = value
      __dispatchAttributes(el, prop, value)
      if (el.getAttribute(prop) !== value) el.setAttribute(prop, value)
    }
  }

  el.attrs = new Proxy({}, AttributesHandler)

  // setAttribute
  const elSetAttribute = el.setAttribute
  el.setAttribute = function () {
    const attrs = C.attributes
    if (attrs) {
      const attr = attrs[arguments[0]]
      if (attr && attr.set) attr.set.apply(this, [arguments[1]])
    }

    elSetAttribute.apply(this, arguments)
    if (!el.attrs[arguments[0]] || el.attrs[arguments[0]] !== arguments[1]) {
      el.attrs[arguments[0]] = arguments[1]
    }
  }

  // getAttribute
  const elGetAttribute = el.getAttribute
  el.getAttribute = function () {
    const attrs = C.attributes
    if (attrs) {
      const attr = attrs[arguments[0]]
      if (attr && attr.get) return attr.get.apply(this)
    }
    return elGetAttribute.apply(this, arguments)
  }

  // =======================================================
  // properties
  const propertiesAssign = []
  for (let prop in C.properties) {
    const value = C.properties[prop]
    if (prop[0] === '$') {
      prop = prop.substr(1)
      el.__$properties.push(prop)
      if (!PROPERTIES[ns][prop]) {
        PROPERTIES[ns][prop] = []
      }
      PROPERTIES[ns][prop].push(el.__$id)
    }
    setProperty(el, prop)

    const data = {
      name: prop,
      value: value
    }
    if (el.data && el.data[prop]) data.value = el.data[prop]
    propertiesAssign.push(data)
  }

  // =======================================================
  // methods
  el.pref = {
    get: (key) => {
      return window.M.preferences.get(CPS[ns][name].__$prefKey + key)
    },
    set: function (key, value) {
      window.M.preferences.set(CPS[ns][name].__$prefKey + key, value)
    },
    has: function (key) {
      return window.M.preferences.set(CPS[ns][name].__$prefKey + key)
    },
    delete: function (key) {
      window.M.preferences.set(CPS[ns][name].__$prefKey + key)
    }
  }
  for (let methodName in C.methods) {
    const rawMethod = methodName
    if (methodName[0] === '$') {
      methodName = methodName.substr(1)
      el.__$methods.push(methodName)
      if (!METHODS[ns][methodName]) {
        METHODS[ns][methodName] = []
      }
      METHODS[ns][methodName].push(el.__$id)
    }

    el[methodName] = _assignMethod(el, rawMethod, 'Method', 'methods')
  }

  // =======================================================
  // DOM remove, contentInsert, create
  if (C.dom) {
    if (C.dom.remove) {
      const elRemove = el.remove
      el.remove = function () {
        try {
          C.dom.remove.apply(this)
        } catch (err) {
          console.linter.postError(err, el, 'domRemove', 'Method')
        }
        elRemove.apply(this)
      }
    }
    if (C.dom.contentInsert) el.contentInsert = _assignMethod(el, 'contentInsert', 'Method', 'dom')
    if (C.dom.create) {
      try {
        C.dom.create.apply(el)
      } catch (err) {
        console.linter.postError(err, el, 'domCreate', 'Method')
      }
    }
  }

  // =======================================================
  // locale
  if (C.locale) {
    el.locale = C.locale
  }

  // =======================================================
  // templating
  if (C.tpls && C.tpls[name]) {
    astToDom(ns, el, el, C.tpls[name].content)
  }

  // =======================================================
  // events
  _preparEvents(el, el.els, C.events)

  // assign properties
  propertiesAssign.forEach(function (props) {
    el[props.name] = props.value
  })

  // dom insert
  if (C.dom && C.dom.insert) {
    el.domInsert = _assignMethod(el, 'insert', 'Method', 'dom')
  }
}

function assign (n, data) {
  if (data) {
    return n.replace(/\$\[([\w.]*)\]/g, function (str, key) {
      const keys = key.split('.')
      let value = data[keys.shift()]
      try {
        keys.forEach(function (val) {
          value = value[val]
        })
        return (value === null || value === undefined) ? '' : value
      } catch (err) {
        return ''
      }
    })
  }
  return n
}

function toAppend (element, tag, position) {
  if (position) {
    if (typeof position === 'string') {
      if (position === 'replace') {
        element.replaceWith(tag)
      } else if (position === 'empty') {
        element.getChildNodes().forEach((el) => {
          el.remove()
        })
        element.insertAdjacentElement('beforeend', tag)
      } else if (tag.nodeType === 1) {
        element.insertAdjacentElement(position, tag)
      } else if (tag.nodeType === 3) {
        element.insertAdjacentText(position, tag.textContent)
      }
    } else {
      position.replaceWith(tag)
    }
  } else {
    element.appendChild(tag)
  }
}

function astToDom (ns, element, refElement, ast, data, position) {
  if (element.data) {
    if (data) {
      data = Object.assign(element.data, data)
    } else if (data !== null) {
      data = element.data
    }
  }
  let siblingContent = false
  const alltag = []
  // refElement for renderTpl
  if (!refElement && position) {
    refElement = alltag
    alltag.els = {}
  }
  for (const el of ast) {
    let tag = null
    switch (el.type) {
      case 'tag': {
        tag = document.createComponent(ns, el.name, data)
        if (el.coulumnStart) {
          tag.coulumnStart = el.coulumnStart
        }
        if (el.attributes) {
          for (const attrName in el.attributes) {
            let value = assign(el.attributes[attrName], data)
            if (attrName === 'el' && refElement) {
              refElement.els[value] = tag
              if (el.attributes[attrName] === 'content' && !Array.isArray(refElement)) {
                siblingContent = true
                tag.__$refparent = refElement
              }
              tag.setAttribute(attrName, value)
            } else if (attrName === 'property' && refElement && !Array.isArray(refElement)) {
              if (value[0] === '$') {
                value = value.substr(1)
              }
              __assignProperties(refElement, value, tag)

              if (data && data[value]) {
                refElement[value] = data[value]
              }
            } else if (attrName === 'attribute' && refElement && !Array.isArray(refElement)) {
              __assignAttributes(refElement, value, tag)
              if (data && data[value]) {
                refElement.setAttribute(value, data[value])
              }
            } else {
              tag.setAttribute(attrName, value)
            }
          }
        }

        if (el.children && el.children.length > 0) {
          astToDom(ns, tag, refElement, el.children, data)
        }
        break
      }
      case 'text': {
        tag = document.createTextNode(assign(el.data, data))
        break
      }
      case 'cdata': {
        const docu = new DOMParser().parseFromString('<xml></xml>', 'application/xml')
        tag = docu.createCDATASection(el.data)
        break
      }
    }
    if (tag) {
      if (element.els &&
        element.els.content &&
        element.els.content !== tag &&
        element.els.content.parentNode !== tag &&
        !siblingContent &&
        position !== 'replace') {
        toAppend(element.els.content, tag, position)
        if (tag.domInsert) tag.domInsert()
      } else {
        toAppend(element, tag, position)
        if (tag.domInsert) tag.domInsert()
      }
      if (element.contentInsert) element.contentInsert()
      if (tag.getAttribute && tag.getAttribute('el')) {
        //  events
        window.M.events.emit(`${ns}--${tag.nodeName}--${tag.getAttribute('el')}`, tag)
      }
      alltag.push(tag)
    }
  }
  return alltag
}

document.createComponent = function (ns, tag, data) {
  const element = document.createElement(tag)
  element.__$ns = ns
  element.__$name = tag
  if (CPS[ns][tag]) {
    element.data = data
    prepareElement(ns, element, tag)
  }
  return element
}

Element.prototype.assignEvents = function (el, els, events) {
  _preparEvents(el, els, events)
}

Element.prototype.getChildNodes = function () {
  if (this.els && this.els.content) {
    return this.els.content.childNodes
  }
  return this.childNodes
}

/*
## trigger(eventName, data)

| Param      | Type     | Description   | Required |
|------------|----------|---------------| -------- |
| eventName  | `string` | event name    | Yes      |
| data       | `mixed`  | sent data     | No       |

```javascript
this.trigger('eventName',12)
```
*/
Element.prototype.trigger = function (eventName, data) {
  const event = new CustomEvent(eventName, {
    detail: data,
    bubbles: true
  })
  this.dispatchEvent(event)
}

Element.prototype.dispatch$ = function (methodName, ...data) {
  if (METHODS[this.__$ns] && METHODS[this.__$ns][methodName]) {
    METHODS[this.__$ns][methodName].forEach(function (id) {
      if (ALLCP[id]) {
        ALLCP[id][methodName].apply(ALLCP[id], data)
      }
    })
  }
}

/*
## getProperties(props)

| Param  | Type                | Description       | Required |
|--------|---------------------|-------------------| -------- |
| props  | `string` or `array` | properties to get | Yes      |

*/
Element.prototype.getProperties = function (props) {
  if (Array.isArray(props) || typeof props === 'string') {
    const result = {}
    if (typeof props === 'string') props = [props]
    for (const prop of props) {
      let properties = getProperties(this.__$ns, prop)
      if (properties.length === 1) {
        properties = properties[0]
      } else if (properties.length === 0) {
        properties = null
      }
      result[prop] = properties
    }
    return result
  }
  console.linter.generateError('SyntaxError', 'Parameter must be a string or an array of strings', 'Method', 'getProperties', this)
  return {}
}

/*
## renderTpl(tplName, data)

| Param     | Type     | Description                                  | Required |
|-----------|----------|----------------------------------------------| -------- |
| tplName   | `string` | template name                                | Yes      |
| data      | `object` | sent data                                    | No       |
| ns        | `string` | force a namespace different from the element | No       |

```javascript
this.renderTpl('tplName',{
key: 'value'
})
```
*/
Element.prototype.renderTpl = function (tplName, data, ns, overlay) {
  if (!ns) {
    ns = this.__$ns
  }
  let tpl
  if (typeof tplName === 'string') {
    if (CPS[ns][this.__$name].tpls && CPS[ns][this.__$name].tpls[tplName]) {
      tpl = CPS[ns][this.__$name].tpls[tplName]
    }
  } else if (typeof tplName === 'object') {
    tpl = tplName
  }

  if (tpl && tpl.content) {
    if ((tpl.overlay && typeof tpl.overlay === 'string') || overlay) {
      const overlayEl = _getOverlay(this, tpl.overlay, overlay)
      if (overlayEl) {
        if (tpl.position && (tpl.position.nodeType || (typeof tpl.position === 'string' && _validPosition.includes(tpl.position)))) {
          if (data && Array.isArray(data)) { // multi render
            const allTags = []
            let els = {}
            for (const d of data) {
              const v = astToDom(ns, overlayEl, undefined, tpl.content, d, tpl.position)
              els = Object.assign(els, v.els)
              delete v.els
              allTags.push(v)
            }
            return { element: allTags, els: els }
          } else { // one render
            const x = astToDom(ns, overlayEl, undefined, tpl.content, data, tpl.position)
            return { element: x[0], els: x.els }
          }
        } else {
          console.linter.generateError('RangeError', `invalid position template definition [${_validPosition.join(', ')}]`, 'Method', 'renderTpl', this)
        }
      } else {
        console.linter.generateError('ReferenceError', `overlay "${tpl.overlay}" does not exist in document`, 'Method', 'renderTpl', this)
      }
    } else {
      console.linter.generateError('SyntaxError', 'invalid overlay template definition', 'Method', 'renderTpl', this)
    }
  } else {
    if (typeof tplName === 'string') {
      console.linter.generateError('ReferenceError', `template "${tplName}" does not exist`, 'Method', 'renderTpl', this)
    } else {
      console.linter.generateError('SyntaxError', 'invalid template', 'Method', 'renderTpl', this)
    }
  }
}

module.exports = {
  addComponents: function addComponents (ns, cps) {
    CPS[ns] = cps
  },
  prepare: function (ns, cps, plugins) {
    ids = 0
    CPS = {}
    CPS[ns] = cps
    METHODS = {}
    METHODS[ns] = {}
    PROPERTIES = {}
    PROPERTIES[ns] = {}
    ALLCP = {}
    const disabledPackages = window.M.preferences.get(`namespace.${ns}.disabledPackages`)
    plugins.forEach((item) => {
      if (CPS[ns][item]) {
        if (!disabledPackages.includes(CPS[ns][item].package)) {
          const plugin = document.createComponent(ns, item)
          plugin.setAttribute('type', 'plugin')
          document.head.insertAdjacentElement('beforeend', plugin)
        }
      }
    })
  },
  start: function (ns) {
    // test if plugin is load or unload
    document.head.querySelectorAll('*[type="plugin"]').forEach(function (plugin) {
      if (plugin.activate) plugin.activate()
    })
    document.body.remove()
    const body = document.createComponent(ns, 'body')
    document.head.insertAdjacentElement('afterEnd', body)
    if (body.domInsert) body.domInsert()
  }
}
