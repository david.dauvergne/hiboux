const path = require('path')
const fs = require('fs')
const mclToAst = require('./mclToAst')
const Module = module.constructor

const loadJSon = function loadJSon (file, ref) {
  try {
    ref.ext = '.json'
    const content = fs.readFileSync(file, 'utf8')
    ref.content = content
    return JSON.parse(content)
  } catch (err) {
    console.linter.parse(err, ref)
  }
  return {}
}

const loadFile = function loadFile (file, ref) {
  try {
    return fs.readFileSync(file, 'utf8')
  } catch (err) {
    console.linter.parse(err, ref)
  }
  return ''
}

const applyLocale = function applyLocale (tpl, params) {
  const names = Object.keys(params)
  const vals = Object.values(params)
  return new Function(...names, `return \`${tpl}\`;`)(...vals)
}

function jsonToCSS (data) {
  return `:root {${Object.entries(data).map(([key, value]) => `${key}:${value}`).join(';')}}`
}

function getNsPath (ns, cpsOrTheme) {
  const pathNamespace = path.join(process.env.APP_USER_DATA, ns)
  if (!fs.existsSync(pathNamespace)) {
    fs.mkdirSync(pathNamespace)
  }
  const pathCpsOrTheme = path.join(pathNamespace, cpsOrTheme)
  if (!fs.existsSync(pathCpsOrTheme)) {
    fs.mkdirSync(pathCpsOrTheme)
  }
  return [
    {
      path: path.join(process.env.APP_DIR_COMPONENTS, ns, cpsOrTheme),
      localization: `app://${ns}/${cpsOrTheme}/`,
      core: true
    },
    {
      path: pathCpsOrTheme,
      localization: `hiboux://${ns}/${cpsOrTheme}/`,
      core: false
    }
  ]
}

function loadTheme (ns, theme) {
  const data = {
    themes: {
      packages: [],
      jsoncss: {},
      locale: {},
      templates: {}
    },
    css: ''
  }
  getNsPath(ns, 'themes').forEach((packageDef) => {
    fs.readdirSync(packageDef.path).forEach(function (themeName) {
      // load package.json
      const packageJsonPath = path.join(packageDef.path, themeName, 'package.json')
      if (fs.existsSync(packageJsonPath)) {
        const packJson = loadJSon(packageJsonPath, {
          file: `${packageDef.localization}${themeName}/package.json`
        })
        packJson.core = packageDef.core
        data.themes.packages.push(packJson)
      }
      if (themeName === theme) {
        // json priority
        let themeFile = path.join(packageDef.path, themeName, themeName + '.json')
        if (fs.existsSync(themeFile)) {
          const themeRoot = loadJSon(themeFile, {
            file: `${packageDef.localization}${themeName}/${themeName}.json`
          })
          data.themes.jsoncss = themeRoot
          data.css += jsonToCSS(themeRoot)
        }
        // css
        themeFile = path.join(packageDef.path, themeName, themeName + '.css')
        if (fs.existsSync(themeFile)) {
          data.css += loadFile(themeFile, {
            file: `${packageDef.localization}${themeName}/${themeName}.css`,
            ext: '.css'
          })
        }
        // templates
        const tplThemesPath = path.join(packageDef.path, themeName, 'templates')
        if (fs.existsSync(tplThemesPath)) {
          fs.readdirSync(tplThemesPath).forEach(function (tpl) {
            const tplPath = path.join(tplThemesPath, tpl)
            const tplFile = path.parse(tplPath)
            const tplRef = {
              file: `${packageDef.localization}${themeName}/templates/${tplFile.base}`,
              ext: tplFile.ext
            }
            if (tplFile.ext === '.css') {
              data.css += loadFile(tplPath, tplRef)
            } else if (tplFile.ext === '.mcl') {
              data.themes.templates[tplFile.name] = loadFile(tplPath, tplRef)
            } else if (tplFile.base === 'locale' && !tplFile.ext) {
              fs.readdirSync(tplPath).forEach(function (langPath) {
                const langFile = path.parse(langPath)
                if (langFile.ext === '.json') {
                  const ref = {
                    file: `${packageDef.localization}${themeName}/templates/locale/${langFile.base}`
                  }
                  data.themes.locale[langFile.name] = loadJSon(path.join(tplPath, langPath), ref)
                }
              })
            }
          })
        }
        if (data.themes.locale && data.themes.locale.en) {
          if (window.LANG !== 'en' && data.themes.locale[window.LANG]) {
            data.themes.locale.locale = Object.assign(data.themes.locale.en, data.themes.locale[window.LANG])
          } else {
            data.themes.locale = data.themes.locale.en
          }
        } else if (data.themes.locale && data.themes.locale[window.LANG]) {
          data.themes.locale = data.themes.locale[window.LANG]
        }
        for (const tplName in data.themes.templates) {
          data.themes.templates[tplName] = mclToAst(
            `${packageDef.localization}${themeName}/templates/${tplName}.mcl`,
            applyLocale(data.themes.templates[tplName], data.themes.locale)
          )
        }
      }
    })
  })
  return data
}

function loadComponents (ns, excludePackages) {
  const data = {
    packages: [],
    cp: {},
    css: '',
    config: {},
    plugin: []
  }
  getNsPath(ns, 'cps').forEach((packageDef) => {
    fs.readdirSync(packageDef.path).forEach(function (packageName) {
      // load package.json
      const packageJsonPath = path.join(packageDef.path, packageName, 'package.json')
      if (fs.existsSync(packageJsonPath)) {
        const packJson = loadJSon(packageJsonPath, {
          file: `${packageDef.localization}${packageName}/package.json`
        })
        packJson.core = packageDef.core
        data.packages.push(packJson)
      }
      if (!excludePackages.includes(packageName)) {
        const packagePath = path.join(packageDef.path, packageName)
        data.config[packageName] = {}
        fs.readdirSync(packagePath).forEach(function (component) {
          const componentPath = path.join(packagePath, component)
          const cp = { js: {}, css: '' }
          let componentName = component
          if (fs.lstatSync(componentPath).isDirectory()) {
            fs.readdirSync(componentPath).forEach(function (file) {
              const filePath = path.join(componentPath, file)
              const f = path.parse(filePath)
              if (f.name === component) {
                const ref = {
                  file: `${packageDef.localization}${packageName}/${component}.${f.ext}`,
                  ext: f.ext
                }
                switch (f.ext) {
                  case '.js':
                    try {
                      var m = new Module()
                      m._compile(loadFile(filePath), __dirname)
                      m.exports.__$prefKey = `namespace.${ns}.packages.${packageName}.${component}.`
                      if (m.exports.config) {
                        data.config[packageName][component] = {
                          title: component,
                          type: 'object',
                          properties: Object.assign({}, m.exports.config)
                        }
                        delete m.exports.config
                      }
                      if (m.exports.plugin) {
                        componentName = 'plugin-' + component
                        data.plugin.push(componentName)
                      }
                      cp.js = m.exports
                    } catch (err) {
                      console.linter.parse(err, ref)
                    }
                    break
                  case '.css':
                    data.css += loadFile(filePath, ref)
                    break
                  case '.mcl':
                    cp.mcl = loadFile(filePath, ref)
                    break
                }
              } else if (f.base === 'locale' && !f.ext) {
                fs.readdirSync(filePath).forEach(function (langFile) {
                  if (!cp.locale) {
                    cp.locale = {}
                  }
                  const l = path.parse(langFile)
                  if (l.ext === '.json') {
                    const ref = {
                      file: `${packageDef.localization}${packageName}/${component}/locale/${l.name}.json`
                    }
                    cp.locale[l.name] = loadJSon(path.join(filePath, langFile), ref)
                  }
                })
              }
            })

            if (cp.locale && cp.locale.en) {
              if (window.LANG !== 'en' && cp.locale[window.LANG]) {
                cp.locale = Object.assign(cp.locale.en, cp.locale[window.LANG])
              } else {
                cp.locale = cp.locale.en
              }
            } else if (cp.locale && cp.locale[window.LANG]) {
              cp.locale = cp.locale[window.LANG]
            }
            if (cp.mcl && cp.locale) {
              cp.mcl = applyLocale(cp.mcl, cp.locale)
            }

            let tpls
            if (cp.mcl) {
              tpls = mclToAst(`${packageDef.localization}${packageName}/${component}.mcl`, cp.mcl)
            }
            if (cp.js) {
              const _cp = { package: packageName }
              if (tpls) {
                _cp.tpls = tpls
              }
              if (cp.locale) {
                _cp.locale = cp.locale
              }
              data.cp[componentName] = Object.assign(cp.js, _cp)
            } else if (tpls) {
              data.cp[componentName] = { tpls: tpls }
            }
          }
        })
      }
    })
  })

  return data
}

module.exports = {
  loadComponents,
  loadTheme
}
