const lexer = require('./mcl/lexer_simple')
const astSimple = require('./mcl/ast_simple')
const PNCpageFnc = require('./mcl/propertiesNames')

// assign properties names
const PNComponent = PNCpageFnc({
  parentNode: '__parent__',
  indent: '__indent__',
  isBlock: '__isBlock__'
})

const _parserComponent = lexer(astSimple(lexer, PNComponent))

module.exports = function (file, content) {
  return _parserComponent(file, content)
}
