const http = require('http')
const pacote = require('pacote')
const semver = require('semver')
const fs = require('fs')
const path = require('path')

const rmdir = function (dirPath) {
  if (fs.existsSync(dirPath)) {
    fs.readdirSync(dirPath).forEach(function (entry) {
      var entryPath = path.join(dirPath, entry)
      if (fs.lstatSync(entryPath).isDirectory()) {
        rmdir(entryPath)
      } else {
        fs.unlinkSync(entryPath)
      }
    })
    fs.rmdirSync(dirPath)
  }
}

function getAllPackages () {
  return new Promise(function (resolve, reject) {
    http.get(process.env.APP_REGISTRY_PACKAGES + '/-/verdaccio/search/.*', (resp) => {
      let data = ''
      resp.on('data', (chunk) => {
        data += chunk
      })
      resp.on('end', () => {
        resolve(JSON.parse(data))
      })
    }).on('error', (err) => {
      reject(err)
    })
  })
}

function getPackageDir (ns, cpsOrTheme) {
  const pathNamespace = path.join(process.env.APP_USER_DATA, ns)
  if (!fs.existsSync(pathNamespace)) {
    fs.mkdirSync(pathNamespace)
  }
  const pathCpsOrTheme = path.join(pathNamespace, cpsOrTheme)
  if (!fs.existsSync(pathCpsOrTheme)) {
    fs.mkdirSync(pathCpsOrTheme)
  }
  return pathCpsOrTheme
}

function installPackage (packageName, ns, cpsOrTheme, cb) {
  const packageDir = path.join(getPackageDir(ns, cpsOrTheme), packageName)
  if (!fs.existsSync(packageDir)) {
    fs.mkdirSync(packageDir)
  }
  pacote.extract(packageName, packageDir, { registry: process.env.APP_REGISTRY_PACKAGES })
    .then(() => {
      cb()
    })
}

function removePackage (packageName, ns, cpsOrTheme, cb) {
  const packageDir = getPackageDir(ns, cpsOrTheme)
  rmdir(path.join(packageDir, packageName))
  cb()
}

function checkVersion (packageName, version, cb) {
  pacote.manifest(packageName, { registry: process.env.APP_REGISTRY_PACKAGES })
    .then((manifest) => cb(semver.gtr(manifest.version, version)))
}

function updatePackage (packageName, ns, cpsOrTheme, cb) {
  removePackage(packageName, ns, cpsOrTheme, () => {
    installPackage(packageName, ns, cpsOrTheme, () => {
      cb()
    })
  })
}

module.exports = {
  getAllPackages,
  installPackage,
  checkVersion,
  removePackage,
  updatePackage
}
