

    namespace
    ├── cps
    │   └── package-x
    │        └── component-x
    │             ├── locale
    │             │   ├── en.json
    │             │   └── fr.json
    │             ├── component-x.css
    │             ├── component-x.js
    │             └── component-x.mcl
    └── package-themes
        └── theme-x
            └── theme-x.css
