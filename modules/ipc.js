const ipc = require('electron').ipcRenderer

const getTag = function (value) {
  return Object.prototype.toString.call(value)
}

const isHas = {
  isObjectStrict: function (value) {
    return typeof value === 'object' && value !== null && typeof value !== 'function' && !Array.isArray(value)
  },
  isString: function (value) {
    return typeof value === 'string' || getTag(value) === '[object String]'
  },
  isSyncFunction: function (value) {
    return getTag(value) === '[object Function]'
  }
}

const onEvents = []

module.exports = {
  removeAllListeners: function () {
    onEvents.forEach((eventName) => {
      ipc.removeAllListeners(eventName)
    })
  },
  on: function (eventName, fnc) {
    if (isHas.isString(eventName)) {
      if (isHas.isSyncFunction(fnc)) {
        if (!onEvents.includes(eventName)) {
          onEvents.push(eventName)
        }
        ipc.on(eventName, fnc)
      } else {
        throw new TypeError('Invalid argument for "on" function, "fnc" argument is undefined or is not a function')
      }
    } else {
      throw new TypeError('Invalid argument for "on" function, "eventName" argument is undefined or is not a string')
    }
  },
  once: function (eventName, fnc) {
    if (isHas.isString(eventName)) {
      if (isHas.isSyncFunction(fnc)) {
        if (!onEvents.includes(eventName)) {
          onEvents.push(eventName)
        }
        ipc.once(eventName, fnc)
      } else {
        throw new TypeError('Invalid argument for "once" function, "fnc" argument is undefined or is not a function')
      }
    } else {
      throw new TypeError('Invalid argument for "once" function, "eventName" argument is undefined or is not a string')
    }
  },
  send: function (eventName, data) {
    if (eventName && isHas.isString(eventName)) {
      ipc.send(eventName, data)
    } else {
      throw new TypeError('Invalid argument for "sendAll" function, "event" argument is undefined or is not a string')
    }
  },
  sendAll: function (message) {
    if (isHas.isObjectStrict(message)) {
      if (message.event && isHas.isString(message.event)) {
        if (ipc.eventNames().includes(message.event)) {
          ipc.send('allWindow', message)
        } else {
          throw new TypeError(`Invalid argument for "sendAll" function, unknown "${message.event}" event`)
        }
      } else {
        throw new TypeError('Invalid argument for "sendAll" function, "event" argument is undefined or is not a string')
      }
    } else {
      throw new TypeError('Invalid argument for "sendAll" function, argument is not an object')
    }
  }
}
