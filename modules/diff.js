const zip = (xs, ys) => {
  const zipped = []
  for (let i = 0; i < Math.max(xs.length, ys.length); i++) {
    zipped.push([xs[i], ys[i]])
  }
  return zipped
}

const arrayIsEgualColumns = (a1, a2) => {
  return a1.coulumnStart.every((x, i) => {
    return x === a2.coulumnStart[i]
  })
}

const diffAttrs = (oldAttrs, newAttrs) => {
  const patches = []

  // set new attributes
  for (const [k, v] of Object.entries(newAttrs)) {
    patches.push((node) => {
      node.setAttribute(k, v)
      return node
    })
  }

  // remove old attributes
  for (const k in oldAttrs) {
    if (!(k in newAttrs)) {
      patches.push((node) => {
        node.removeAttribute(k)
        return node
      })
    }
  }

  return (node) => {
    for (const patch of patches) {
      patch(node)
    }
  }
}

const diffChildren = (oldVChildren, newVChildren) => {
  const childPatches = []
  oldVChildren.forEach((oldVChild, i) => {
    childPatches.push(diff(oldVChild, newVChildren[i]))
  })

  const additionalPatches = []
  for (const additionalVChild of newVChildren.slice(oldVChildren.length)) {
    additionalPatches.push((node) => {
      node.renderTpl({
        overlay: ':scope',
        position: 'beforeend',
        content: [additionalVChild]
      })
      return node
    })
  }

  return (parent) => {
    for (const [patch, child] of zip(childPatches, parent.getChildNodes())) {
      patch(child)
    }

    for (const patch of additionalPatches) {
      patch(parent)
    }

    return parent
  }
}

const replaceNode = (vNewNode) => {
  return (node) => {
    let element
    if (vNewNode.type === 'tag') {
      ({ element } = node.renderTpl({
        overlay: ':scope',
        position: 'replace',
        content: [vNewNode]
      }))
    } else {
      ({ element } = node.parentNode.renderTpl({
        overlay: ':scope',
        position: node,
        content: [vNewNode]
      }))
    }
    return element
  }
}

const diff = (vOldNode, vNewNode) => {
  if (vNewNode === undefined) {
    return (node) => {
      node.remove()
      return undefined
    }
  }
  if (vOldNode.type !== vNewNode.type) {
    return replaceNode(vNewNode)
  } else if (vNewNode.type !== 'tag') {
    if (vOldNode.data !== vNewNode.data) {
      return replaceNode(vNewNode)
    } else {
      return (node) => undefined
    }
  } else if (vOldNode.name !== vNewNode.name) {
    return replaceNode(vNewNode)
  } else if (!arrayIsEgualColumns(vOldNode, vNewNode)) {
    return replaceNode(vNewNode)
  }

  const patchAttrs = diffAttrs(vOldNode.attributes, vNewNode.attributes)
  const patchChildren = diffChildren(vOldNode.children, vNewNode.children)

  return (node) => {
    patchAttrs(node)
    patchChildren(node)
    return node
  }
}

module.exports = diff
