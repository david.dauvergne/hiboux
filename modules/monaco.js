
var cbs = []
var monacoIs = false
require('../lib//monaco').then(function (monaco) {
  monacoIs = monaco
  cbs.forEach(function (cb) {
    cb(monacoIs)
  })
})

module.exports = function (cb) {
  if (monacoIs) {
    cb(monacoIs)
  } else {
    cbs.push(cb)
  }
}
